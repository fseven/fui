import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import fui 1.0
ApplicationWindow {
    id: _root
    visible: true  
    // 设置全屏模式
    visibility: Window.Maximized

    // 隐藏标题栏
//    flags: Qt.Window | Qt.FramelessWindowHint

    // 设置窗口大小随屏幕大小变化而变化
    maximumWidth: Screen.width
    maximumHeight: Screen.height
    MouseArea{
        enabled: false
        anchors.fill: parent
        onWheel: {
            console.log(wheel.angleDelta.y)
            let scrollSpeed = 12
            let delta = wheel.angleDelta.y
            if (delta > 0 && _scrollBar.position > 0) {  // 向上滚动
                // 在此添加你的向上滚动代码...
                _scrollBar.position -= 0.01;
            } else if (delta < 0 && _scrollBar.position < (1 - _scrollBar.size)) {  // 向下滚动
                // 在此添加你的向下滚动代码...
                _scrollBar.position += 0.01;
            }
        }
    }

    ScrollBar{
        id: _scrollBar
        anchors.right: parent.right
        anchors.rightMargin: 2
        active: true
        visible: true
        hoverEnabled: true
        orientation: Qt.Vertical
        size: 0.2
        interactive: true
        contentItem: Rectangle {
            implicitWidth: 6
            implicitHeight: 100
            radius: width / 2
            color: _scrollBar.hovered ? Qt.rgba(144/255,147/255,153/255,0.5) : Qt.rgba(144/255,147/255,153/255,0.3)
        }
        onHoveredChanged: {
            if(hovered){
                _mouseArea.cursorShape = Qt.PointingHandCursor
            } else{
                _mouseArea.cursorShape = Qt.ArrowCursor
            }
        }
        MouseArea{
            id: _mouseArea
            enabled: false
            anchors.fill: parent
        }
    }

    MainPage{
        property var scrollBar: {
            Singleton.contentScrollBar = _scrollBar
            return Singleton.contentScrollBar
        }
        width: 1140
        anchors.centerIn: parent
        height: parent.height
    }
}
