//用来写Readme之类的
import QtQuick 2.12
import QtQuick.Controls 2.12
FocusScope{
    id: _basePage
    objectName: "basePage"
    default property alias contentItem: _focusScope.data
    property string backgroundImage: ""

    Control{
        background: Image{
            source: backgroundImage
        }
        contentItem: FocusScope{
            id: _focusScope
            focus: true
        }
    }
}
