//用来写Readme之类的
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQml 2.12
FocusScope{
    id: _basePage
    objectName: "basePage"
    default property alias contentItem: _focusScope.data
    property string backgroundImage: ""

    Control{
        background: Image{
            source: backgroundImage
        }
        contentItem: FocusScope{
            id: _focusScope
            focus: true
            TextArea{
                id: _textEdit
                wrapMode: Text.WrapAnywhere
                textFormat: Qt.RichText
                readOnly: true
                selectByMouse: true
                font.pixelSize: 12
                topPadding: 18
                bottomPadding: 18
                leftPadding: 24
                rightPadding: 24
                // 当鼠标移动到链接上时触发
                onLinkActivated: {
                    Qt.openUrlExternally(link)
                }
                Component.onCompleted: {
                    $app.setDefaultStyleSheet(_textEdit.textDocument)
                    _textEdit.text = fileManager.readFile(":/home/html/readme.html",false)
                }
            }
        }
    }
}
