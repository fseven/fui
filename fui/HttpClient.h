#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QNetworkAccessManager>
#include <QObject>
#include <Singleton.hpp>
#include "HeadParameter.h"

class QNetworkRequest;
class QNetworkReply;

namespace FUI{
class HttpClient : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(HttpClient)
    friend Singleton<HttpClient>;
private:
    explicit HttpClient(QObject *parent = nullptr);
    QVector<QString> getHeaders();
public:
    void setRequestHeadParameter(const HeadParameter &headParameter);
    void get(const QNetworkRequest &request, QNetworkReply *reply);
private:
    HeadParameter headParameter;
    QScopedPointer<QNetworkAccessManager> accessManager;
signals:

};
}


#endif // HTTPCLIENT_H
