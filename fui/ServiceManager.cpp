#include <QScopedPointer>
#include <QTimer>
#include <QtWebSockets/QWebSocket>
#include "ServiceManager.h"
#include "EventHandler.h"
#include "protocols/ServiceProtocol.h"
#include <QtConcurrent>

using namespace FUI;
typedef std::string Event;

class ServiceManagerPrivate{
public:
    ServiceManagerPrivate():
        webSocket(new QWebSocket())
    {        
        server.setScheme("ws");
    }
    QScopedPointer<QWebSocket> webSocket;
//    QNetworkRequest request;
    QUrl server;
    const int reconnectInterval = 5000; // 重连间隔（毫秒）
    EventHandler handle;

};

FUI::ServiceManager::ServiceManager(QObject *parent)
    : QObject(parent),
      d_ptr(new ServiceManagerPrivate())
{
    Q_D(ServiceManager);    
    connect(d->webSocket.get(), &QWebSocket::connected, this, &FUI::ServiceManager::onConnected);
    connect(d->webSocket.get(), &QWebSocket::disconnected, this, &FUI::ServiceManager::onDisconnected);
    connect(d->webSocket.get(), QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error), this, &FUI::ServiceManager::onError);
    connect(d->webSocket.get(), &QWebSocket::textMessageReceived, this, &FUI::ServiceManager::onTextMessageReceived);    
}

void FUI::ServiceManager::connectServer()
{
    Q_D(ServiceManager);
    connectServer(d->server);
}

void FUI::ServiceManager::connectServer(const QUrl &url)
{
    Q_D(ServiceManager);
    if(url.isEmpty()){
        qDebug()<<"connectServer url is empty!";
        return;
    }

    if(d->server != url){
        d->server.setPort(url.port());
        d->server.setHost(url.host());
    }
    qDebug()<<"connectServer: "<<url.url();

    d->webSocket->open(d->server);
}

void FUI::ServiceManager::appendListener(const std::string &event,Callback callback)
{
    Q_D(ServiceManager);    
    d->handle.appendListener(event,callback);
}

bool ServiceManager::isConnected()
{
    Q_D(ServiceManager);
    return d->webSocket->state() == QAbstractSocket::ConnectedState;
}

void ServiceManager::sendMessage(const QByteArray &msg)
{
    Q_D(ServiceManager);
    d->webSocket->sendTextMessage(msg);
}

void FUI::ServiceManager::onConnected()
{
    Q_D(ServiceManager);
    qDebug()<<QString("连接成功 ip:%1   port:%2")
              .arg(d->server.url())
              .arg(d->server.port());
}

void FUI::ServiceManager::onClosed()
{
    Q_D(ServiceManager);
    qDebug()<<QString("已关闭连接 ip:%1   port:%2")
              .arg(d->server.url())
              .arg(d->server.port());
}

void FUI::ServiceManager::onError()
{
    Q_D(ServiceManager);
    qDebug() << "连接失败，重试中...";
    // 连接失败后重新连接
    qDebug()<< d->webSocket->errorString();

    connectServer();
}

void FUI::ServiceManager::onDisconnected()
{
    Q_D(ServiceManager);
    qDebug() << "连接已断开...";
    // 连接失败后重新连接
    if (d->webSocket->state() == QAbstractSocket::UnconnectedState) {
        QTimer::singleShot(d->reconnectInterval, this, [this](){
            connectServer();
        });
      }
}

void FUI::ServiceManager::onTextMessageReceived(const QString &message)
{
    Q_D(ServiceManager);
    qDebug()<<"onTextMessageReceived: "<<message;

    // 处理其他消息
    ServiceProtocol<SEmptyData> baseProtocol;
    baseProtocol.ParseJsonString(message.toUtf8());
    Event event = baseProtocol.m_event.toStdString();
    d->handle.dispatch(event,message.toStdString());
}
