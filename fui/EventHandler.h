#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include "eventdispatcher.h"
#include "mixins/mixinfilter.h"
#include <iostream>
#include <functional>

class EventHandler
{    
    typedef std::string           Event;
    typedef std::string           Body;
    typedef std::function<void (const std::string &)> Callback;
    typedef eventpp::EventDispatcher<Event,void (const std::string &)> EventEmitter;
    typedef std::shared_ptr<EventEmitter> EventEmitterPtr;
public:
    EventHandler();
    // 订阅事件
    void appendListener(Event event, Callback callback);
    void dispatch(Event event,Body body);

private:
    EventEmitterPtr dispatcher;
};

#endif // EVENTHANDLER_H
