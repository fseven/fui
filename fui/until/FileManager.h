#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include "Singleton.hpp"

class QJSValue;
class QMutex;
namespace FUI {
class FileManager : public QObject
{
    Q_OBJECT
private:
    friend Singleton<FileManager>;
    explicit FileManager(QObject *parent = nullptr);
public:

    /**
     * @brief 异步读取文件，并将结果返回到callback函数中
     * @param callback
     */
    Q_INVOKABLE void readFileAsync(const QString &fileName, const QJSValue &callback);
    Q_INVOKABLE QString readFile(const QString &fileName,bool format2HTML = true);
    Q_INVOKABLE void createDirWithCascade(const QString &dirPath);
    Q_INVOKABLE void deleteFile(const QString &filePath);
    ~FileManager();
signals:

public slots:

private:
    uint add(const QJSValue &callback);
    void remove(uint key);
    uint observerIds = 0;
    QMap<uint,QJSValue> *callbacks;
    QMutex *mutex;
};
}

#endif // FILEMANAGER_H
