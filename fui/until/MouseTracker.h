#ifndef MOUSETRACKER_H
#define MOUSETRACKER_H

#include <QObject>
#include "Singleton.hpp"

//监控鼠标的位置
namespace FUI {
class MouseTracker : public QObject
{
    Q_OBJECT
private:
    friend Singleton<MouseTracker>;
    Q_DISABLE_COPY(MouseTracker)
    explicit MouseTracker(QObject *parent = nullptr);
public:

signals:
    //发送鼠标此时所处的位置
    void positionChanged(const QPoint &pos);

protected:
    bool eventFilter(QObject *watched, QEvent *event) override;
};
}

#endif // MOUSETRACKER_H
