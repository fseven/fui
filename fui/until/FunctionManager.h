#ifndef FUNCTIONMANAGER_H
#define FUNCTIONMANAGER_H

#include <functional>

namespace FUI {
class FunctionManager
{
    // 定义函数指针类型
    using FunctionPtr = std::function<void()>;
public:
    FunctionManager();
    ~FunctionManager();

    // 添加函数映射
    void add(const std::string& key,FunctionPtr&& function);

    // 执行函数
    bool execute(const std::string& key);
private:
    std::unordered_map<std::string, FunctionPtr> functionMap;
};
}

#endif // FUNCTIONMANAGER_H
