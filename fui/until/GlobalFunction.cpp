#include <QQmlEngine>
#include <QQmlContext>
#include <QDebug>
#include "GlobalFunction.h"
#include "Singleton.hpp"
#include "FNamespace.h"
#include "MouseTracker.h"
#include "FileManager.h"
#include "AppGlobal.h"

#define SetContextProperty(NAME,VALUE) \
    {const auto &context = engine->rootContext();\
    context->setContextProperty(NAME,VALUE);}

void FUI::registerManager(QQmlEngine *engine){
    if(!engine){
        qWarning()<<"engine is nullptr";
        return;
    }
    SetContextProperty("mouseTracker",&Provider(FUI::MouseTracker));
    SetContextProperty("fileManager",&Provider(FUI::FileManager));
    SetContextProperty("$app",&Provider(FUI::AppGlobal));
    qmlRegisterType<FUI::GlobalNameSpace>("fui",1,0,"FUI");    
}
