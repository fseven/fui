﻿#include <QObject>
namespace FUI {
class GlobalNameSpace: public QObject {
    Q_OBJECT
public:
    GlobalNameSpace(QObject *parent = nullptr):QObject(parent){}

    enum ButtonType {
        Default,
        Primary,
        Success,
        Info,
        Warning,
        Danger
    };
    enum class Direction{
        Vertical,
        Horizontal
    };

    QString valueToKey(GlobalNameSpace::ButtonType type);

    Q_ENUM(Direction)
    Q_ENUM(ButtonType)
};

}
