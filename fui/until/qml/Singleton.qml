pragma Singleton
import QtQuick 2.12
import QtQml 2.12
import route 1.0

QtObject {
    id: _globalObject

    //全局的ScrollBar，与右侧内容的scrollBar保持同步        
    property var contentScrollBar
    // 在这里可以添加其他属性、方法或信号
    property var leftAreaData: [
        {title:"Basic",items:[
                {
                    name:"Button 按钮",
                    onClicked:function(){
                        let buttonPage = Route.getPage("buttonPage")
                        if(buttonPage){
                            console.log("buttonPage 1")
                            Route.popTo("buttonPage")
                        } else{
                            console.log("buttonPage 2")
                            Route.push(Route.buttonPage)
                        }
                        console.log("Button 按钮")
                    }
                },
                {
                    name:"Link 文字链接",
                    onClicked:function(){
                        console.log("Link 文字链接")
                    }
                }
            ]
        },
        {"title":"Form",items:[
                {
                    name:"Radio 单选框",
                    onClicked:function(){}
                },
                {
                    name:"CheckBox 多选框",
                    onClicked:function(){}
                },
                {
                    name:"Input 输入框",
                    onClicked:function(){}
                },
                {
                    name:"InputNumber 计数器",
                    onClicked:function(){}
                },
                {
                    name:"Select 选择器",
                    onClicked:function(){}
                },
            ]
        },
        {"title":"Form",items:[
                {
                    name:"Radio 单选框",
                    onClicked:function(){}
                },
                {
                    name:"CheckBox 多选框",
                    onClicked:function(){}
                },
                {
                    name:"Input 输入框",
                    onClicked:function(){}
                },
                {
                    name:"InputNumber 计数器",
                    onClicked:function(){}
                },
                {
                    name:"Select 选择器",
                    onClicked:function(){}
                },
            ]
        },
        {"title":"Form",items:[
                {
                    name:"Radio 单选框",
                    onClicked:function(){}
                },
                {
                    name:"CheckBox 多选框",
                    onClicked:function(){}
                },
                {
                    name:"Input 输入框",
                    onClicked:function(){}
                },
                {
                    name:"InputNumber 计数器",
                    onClicked:function(){}
                },
                {
                    name:"Select 选择器",
                    onClicked:function(){}
                },
            ]
        },
        {"title":"Form",items:[
                {
                    name:"Radio 单选框",
                    onClicked:function(){}
                },
                {
                    name:"CheckBox 多选框",
                    onClicked:function(){}
                },
                {
                    name:"Input 输入框",
                    onClicked:function(){}
                },
                {
                    name:"InputNumber 计数器",
                    onClicked:function(){}
                },
                {
                    name:"Select 选择器",
                    onClicked:function(){}
                },
            ]
        }
    ]
}
