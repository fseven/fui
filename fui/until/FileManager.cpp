#include "FileManager.h"
#include "thread"
#include <QFile>
#include <QJSValue>
#include <QMap>
#include <QMutex>
#include <QDebug>
#include <QDir>

FUI::FileManager::FileManager(QObject *parent) : QObject(parent),
    callbacks(new QMap<uint,QJSValue>()),
    mutex(new QMutex())
{

}

void FUI::FileManager::readFileAsync(const QString &fileName,const QJSValue &callback)
{
    QMutexLocker locker(mutex);
    const auto &observerId = this->add(callback);
    std::thread([=](){
        //read file
        const auto &content = readFile(fileName);

        if(callbacks->contains(observerId)){
            auto fun = callbacks->find(observerId);
            QMetaObject::invokeMethod(this,[=](){
                QJSValueList param;
                param<<content;
                fun->call(param);
                remove(observerId);
            },Qt::QueuedConnection);
        }
    }).detach();
}

QString FUI::FileManager::readFile(const QString &fileName, bool format2HTML)
{
    QFile file(fileName);
    if(file.exists()){
        if(file.open(QIODevice::ReadOnly)){
            if(format2HTML){
                return file.readAll()
                        .replace("\n", "<br>")
                        .replace("\t", "&#9;")
                        .replace(" ", "&nbsp;");
            } else{
                return file.readAll();
            }
        } else{
            qWarning()<<"file can't open !!!";
            return QString();
        }
    }else {
        qWarning()<<"file is not exists !!!";
        return QString();
    }
}

void FUI::FileManager::createDirWithCascade(const QString &dirPath)
{
    QDir dir;
    if(!dir.exists(dirPath)){
        bool created = dir.mkpath(dirPath);
        if (created) {
            qDebug() << "Directory created successfully.";
        } else {
            qDebug() << "Failed to create directory.";
        }
    } else{
        qDebug() << QString("Directory exists:%1").arg(dirPath);
    }
}

void FUI::FileManager::deleteFile(const QString &filePath)
{

    QFile::remove(filePath);
}

FUI::FileManager::~FileManager()
{
    if(callbacks){
        callbacks->clear();
        delete callbacks;
        callbacks = nullptr;
    }

    if(mutex){
        delete mutex;
        mutex = nullptr;
    }
}

uint FUI::FileManager::add(const QJSValue &callback)
{
    callbacks->insert(observerIds,callback);
    return observerIds++;
}

void FUI::FileManager::remove(uint key)
{
    callbacks->remove(key);
}
