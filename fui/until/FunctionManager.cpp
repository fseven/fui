#include "FunctionManager.h"


FUI::FunctionManager::FunctionManager() {

}

FUI::FunctionManager::~FunctionManager()
{
    functionMap.clear();
}

void FUI::FunctionManager::add(const std::string &key, FunctionPtr &&function) {
    functionMap[key] = function;
}

// 执行函数
bool FUI::FunctionManager::execute(const std::string& key) {
    if (functionMap.find(key) != functionMap.end()) {
        functionMap.at(key)();
        return true;
    } else {
        return false;
    }
}
//// 示例函数
//void func1(const std::string& arg) {
//    std::cout << "Function 1: " << arg << std::endl;
//}

//void func2(int arg) {
//    std::cout << "Function 2: " << arg << std::endl;
//}

//int main() {
//    FunctionManager functionManager;

//    // 添加函数映射
//    functionManager.add("one", [](){
//        func1("a");
//    });
//    functionManager.add("two", [](){
//        func2(2);
//    });

//    // 执行函数
//    functionManager.execute("one");
//    functionManager.execute("two");

//    return 0;
//}
