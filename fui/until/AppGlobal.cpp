#include "AppGlobal.h"
#include "FileManager.h"
#include <QQuickTextDocument>

#include <QPixmap>
#include <QPainter>
#include <QSvgRenderer>
#include <QBrush>
#include <QPixmap>

FUI::AppGlobal::AppGlobal(QObject *parent) : QObject(parent)
{

}

void FUI::AppGlobal::setDefaultStyleSheet(QQuickTextDocument *qd)
{        
    auto td = qd->textDocument();
    qDebug()<< td->defaultStyleSheet();
    td->setDefaultStyleSheet(Provider(FileManager).readFile(":/home/css/markdown.css",false));
    qDebug()<< td->defaultStyleSheet();
}

QIcon FUI::AppGlobal::changeSVGColor(const QString& path,const QColor& color)
{
    QPixmap pixmap;

    QPainter painter(&pixmap);
    painter.setBrush(QBrush(color));

    QSvgRenderer renderer(path);
    pixmap.scaled(renderer.defaultSize());
    pixmap.fill(Qt::transparent);

    renderer.render(&painter);

    return QIcon(pixmap);
}
