#ifndef FLOG_H
#define FLOG_H
#include <QObject>
#include "Singleton.hpp"
#include "spdlog/spdlog.h"

namespace FUI {
class FLog : public QObject
{
    Q_OBJECT
private:
    friend Singleton<FLog>;
    Q_DISABLE_COPY(FLog)
public:
    explicit FLog(QObject *parent = nullptr);
    static void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);
    /**
     * 初始化日志
     */
    void initLog();

    /**
     * 初始化日志
     * @param path 工程主目录 需要全路径，比如：/root/fui/application/
     * @param level 日志级别
     */
    void initLog(QString path, int level);
private:
    void info(const QMessageLogContext& context, const QString& msg);
    void debug(const QMessageLogContext& context, const QString& msg);
    void trace(const QMessageLogContext& context, const QString& msg);
    void warn(const QMessageLogContext& context, const QString& msg);
    void error(const QMessageLogContext& context, const QString& msg);
    void critical(const QMessageLogContext& context, const QString& msg);
    inline std::shared_ptr<spdlog::logger> getLogger(){
        return m_logger;
    }
    std::string getSuffix(const QString &msg,const QMessageLogContext& context,const QString &endFlag="");
private:
    std::shared_ptr<spdlog::logger> m_logger;
    std::shared_ptr<spdlog::details::thread_pool> m_pThreadPool;
signals:

};
}
#endif // FLOG_H
