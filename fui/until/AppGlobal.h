#ifndef APPGLOBAL_H
#define APPGLOBAL_H

#include <QObject>
#include "Singleton.hpp"
#include <QIcon>

class QQuickTextDocument;

namespace FUI {
class AppGlobal : public QObject
{
    Q_OBJECT
private:
    Q_DISABLE_COPY(AppGlobal)
    friend Singleton<AppGlobal>;
    explicit AppGlobal(QObject *parent = nullptr);
public:
    Q_INVOKABLE void setDefaultStyleSheet(QQuickTextDocument *qd);


    /**
     * @brief changeSVGColor
     * @param path: 图片路径
     * @param color: 颜色
     * @return
     */
    Q_INVOKABLE QIcon changeSVGColor(const QString& path, const QColor& color);
signals:

};
}

#endif // APPGLOBAL_H
