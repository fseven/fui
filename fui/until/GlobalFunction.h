#ifndef GLOBALFUNCTION_H
#define GLOBALFUNCTION_H
//全局功能函数汇总
class QQmlEngine;
class QString;
namespace FUI{
void registerManager(QQmlEngine *engine); // 前置声明函数

// 定义函数指针类型
#include <functional>
template <typename... Args>
using FunctionPtr = std::function<void(Args...)>;

template <typename... Args>
void executeFunction(const FunctionPtr<Args...>& function, Args&&... args) {
    function(std::forward<Args>(args)...);
}
}

#endif // GLOBALFUNCTION_H
