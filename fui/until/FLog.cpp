#include "FLog.h"
#include <iostream>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/ansicolor_sink.h>
#include "spdlog/async.h"
#include "spdlog/spdlog.h"

#include <QDir>
#include <QGuiApplication>
#include <QDebug>
#include <QVector>
#include <QTextCodec>

std::string FUI::FLog::getSuffix(const QString &msg,const QMessageLogContext& context,const QString &endFlag){
    const auto &fileName= context.file == nullptr ? "-" : context.file;
    const auto &functionName= context.function == nullptr ? "-" : context.function;
    const auto &r = QString("[%1/%2/%3] %4%5").arg(fileName).arg(functionName).arg(context.line).arg(msg).arg(endFlag);
    QTextCodec* codec = QTextCodec::codecForLocale();
    const auto &c = codec->fromUnicode(r);
    auto str = std::string(c.constData(),c.length()) ;
    return str;
}

FUI::FLog::FLog(QObject *parent) : QObject(parent)
{

}

void FUI::FLog::info(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->info("{}",str);
}

void FUI::FLog::debug(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->debug("{}",str);
}

void FUI::FLog::trace(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->trace("{}",str);
}

void FUI::FLog::warn(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->warn("{}",str);
}

void FUI::FLog::error(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->error("{}",str);
}

void FUI::FLog::critical(const QMessageLogContext &context, const QString &msg)
{
    const auto &str = getSuffix(msg,context,"");
    this->getLogger()->critical("{}",str);
}

void FUI::FLog::messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    auto &log = Provider(FUI::FLog);
    switch (type) {
      case QtDebugMsg:
        log.debug(context,msg);
        break;
      case QtInfoMsg:
        log.info(context,msg);
        break;
      case QtWarningMsg:
        log.warn(context,msg);
        break;
      case QtCriticalMsg:
        log.critical(context,msg);
        break;
      case QtFatalMsg:
        log.error(context,msg);
        break;
    }
}

void FUI::FLog::initLog()
{
    initLog(qApp->applicationDirPath(), spdlog::level::trace);
}

void FUI::FLog::initLog(QString absolutePath, int level)
{
    if(absolutePath.isEmpty()) {
        absolutePath.append(qApp->applicationDirPath());
    }

    if(absolutePath.at(absolutePath.size() - 1) != '/') {
        absolutePath = absolutePath + "/";
    }

    absolutePath.append("log/");

    QDir dir;
    if(!dir.exists(absolutePath)){
        bool created = dir.mkpath(absolutePath);
        if (created) {
            qDebug() << "Directory created successfully.";
        } else {
            qDebug() << "Failed to create directory.";
        }
    }

    absolutePath.append("log.txt");
    QFile::remove(absolutePath);
    try {
        //设置日志等级
//        spdlog::set_level(static_cast<spdlog::level::level_enum>(level));

        // default thread pool settings can be modified *before* creating the async logger:
        //spdlog::init_thread_pool(1024, 1);
//        m_pThreadPool = std::make_shared<spdlog::details::thread_pool>(1024, 1);
        m_pThreadPool = std::make_shared<spdlog::details::thread_pool>(1024,1);

        auto console_sink = std::make_shared<spdlog::sinks::ansicolor_stderr_sink_mt>();
        console_sink->set_level(spdlog::level::trace);//console这个设置好像没有用

//        auto sys_sink = std::make_shared<spdlog::sinks::syslog_sink_mt >("fui", qApp->applicationPid() , 1, true);
//        sys_sink->set_level(spdlog::level::trace);

        // Create a file rotating logger with 5mb size max and 3 rotated files
        //m_logger = spdlog::rotating_logger_mt<spdlog::async_factory>(loggerName, fileName, 1024 * 1024 * 5, 5);
        auto rotating_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(absolutePath.toStdString(), 1024*1024*5, 1, true);
        rotating_sink->set_level(spdlog::level::trace);

       std::vector<spdlog::sink_ptr> sinks {console_sink, rotating_sink};
//        spdlog::thread_pool()
        //如果有多个so库都是用spdlog，那必需每个so内都自己用自己的m_pThreadPool，不然会产生崩溃
        m_logger = std::make_shared<spdlog::async_logger>("fui", sinks.begin(), sinks.end(), m_pThreadPool, spdlog::async_overflow_policy::block);

        spdlog::register_logger(m_logger);

        m_logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%n] [%l] [%t] %v%$");
        //当遇到错误级别以上的立刻刷新到日志
        m_logger->flush_on(spdlog::level::err);
        //每三秒刷新一次
        //spdlog::flush_every(std::chrono::seconds(3));

        //这个要放到最后,不然debug和trace不会输出
        m_logger->set_level(static_cast<spdlog::level::level_enum>(level));
    }
    catch (const spdlog::spdlog_ex &ex) {
        qDebug() << "FLog initialization failed: " << ex.what();
    }
}
