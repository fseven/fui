#ifndef JSONTOOLS_H
#define JSONTOOLS_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QDebug>

//将对象格式化为json格式
#define FORMAT_JSON_OBJECT_BEGIN \
    QByteArray FormatJsonToString() \
    { \
        QJsonDocument jsonDoc; \
        QJsonObject jsonObject = jsonDoc.object();

#define FORMAT_JSON_SUB_OBJECT_BEGIN \
    void FormatJsonToString(QJsonObject &jsonObject) \
    { \
        (void)jsonObject;

#define FORMAT_JSON_NUMBER(name) \
        jsonObject[#name] = m_##name;\

#define FORMAT_JSON_STRING(name) \
        jsonObject[#name] = m_##name;\

#define FORMAT_JSON_BOOL(name) \
        if (m_##name) { \
            jsonObject[#name] = true;\
        }\
        else { \
            jsonObject[#name] = false;\
        }

#define FORMAT_JSON_LIST(name) \
        QJsonArray name##JsonArray; \
        for (auto it = m_##name.begin(); it != m_##name.end(); ++it) { \
            name##JsonArray.push_back(*it); \
        } \
        jsonObject[#name] = name##JsonArray;

#define FORMAT_JSON_STRING_LIST(name) \
        QJsonArray name##JsonArray; \
        for (auto it = m_##name.begin(); it != m_##name.end(); ++it) { \
            name##JsonArray.push_back(*it); \
        } \
        jsonObject[#name] = name##JsonArray;

#define FORMAT_JSON_OBJECT(name) \
        QJsonObject name##Obj; \
        m_##name.FormatJsonToString(name##Obj); \
        jsonObject[#name] = name##Obj;

/*
* 这里用auto类型，是容器类型的可以通用，否则的话就需要分别定义list，vector，set等等容器类型了
*/
#define FORMAT_JSON_OBJECT_LIST(name, type) \
        QJsonArray name##ObjArray; \
        for (auto it = m_##name.begin(); it != m_##name.end(); ++it) { \
            QJsonObject name##Obj; \
            it->FormatJsonToString(name##Obj); \
            name##ObjArray.push_back(name##Obj); \
        } \
        jsonObject[#name] = name##ObjArray; \

#define FORMAT_JSON_SUB_OBJECT_END \
    }

#define FORMAT_JSON_OBJECT_END \
        jsonDoc.setObject(jsonObject); \
        auto formatJsonStr = jsonDoc.toJson(QJsonDocument::Compact); \
        return formatJsonStr; \
    }


//解析json，检查是否含有某个成员 (仅限一级层次结构下的判断)
#define CHECK_JSON_HAS_MEMBER \
    bool CheckJsonHasMember(const QByteArray &jsonStr, const QString &name) \
    { \
        QJsonParseError *error = nullptr; \
        const auto &jsonDoc = QJsonDocument::fromJson(jsonStr,error); \
        if(jsonDoc.isNull() && error){ \
            qDebug()<<"Json Parse error:"<< error->errorString()<<", Error json:" << jsonStr; \
            return false; \
        } \
        return jsonDoc.object().contains(name); \
    }

//解析json，检查某个json对象下，是否含有某个成员 (json对象仅限一级层次结构下)
#define CHECK_JSON_OBJECT_HAS_MEMBER \
    bool CheckJsonObjectHasMember(const QByteArray &jsonStr, const QString &objName, const QString &name) \
    { \
        QJsonParseError *error = nullptr; \
        const auto &jsonDoc = QJsonDocument::fromJson(jsonStr,error); \
        if(jsonDoc.isNull() && error){ \
            qDebug()<<"Json Parse error:"<< error->errorString()<<", Error json:" << jsonStr; \
            return false; \
        } \
        const auto &jsonObj = jsonDoc.object(); \
        if(jsonObj[objName].isObject()){ \
            return false; \
        } \
        return jsonObj[objName].toObject().contains(name); \
    }


//解析json格式字符串到对象中
#define PARSE_JSON_OBJECT_BEGIN \
    bool ParseJsonString(const QByteArray &jsonStr) \
    { \
        QJsonParseError *error = nullptr; \
        const auto &jsonDoc = QJsonDocument::fromJson(jsonStr,error); \
        if(jsonDoc.isNull() && error){ \
            qDebug()<<"Json Parse error:"<< error->errorString()<<", Error json:" << jsonStr; \
            return false; \
        }\
        QJsonObject jsonObj = jsonDoc.object();

#define PARSE_JSON_SUB_OBJECT_BEGIN \
    bool ParseJsonString(const QJsonObject &jsonObj) \
    { \
        (void)jsonObj;
#define PARSE_JSON_INT(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toInt(); \
        }

#define PARSE_JSON_INT64(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toLongLong(); \
        }

#define PARSE_JSON_UINT(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toUInt(); \
        }

#define PARSE_JSON_UINT64(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toULongLong(); \
        }

#define PARSE_JSON_FLOAT(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toFloat(); \
        }

#define PARSE_JSON_DOUBLE(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toDouble(); \
        }


#define PARSE_JSON_STRING(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toString(); \
        }

#define PARSE_JSON_BOOL(name) \
        if (jsonObj.contains(#name)) { \
            m_##name = jsonObj[#name].toVariant().toBool(); \
        }

#define PARSE_JSON_INT_LIST(name) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##Value; \
            name##Value = jsonDoc[#name]; \
            if (name##Value.isArray()) { \
                const auto &name##Array = name##Value.toArray(); \
                if(name##Array.isEmpty()){ \
                    for(auto i = 0; i < name##Array.size(); i++){ \
                        m_##name.push_back(name##Value[i].toVariant().toInt()); \
                    } \
                } \
            } \
        }

#define PARSE_JSON_DOUBLE_LIST(name) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##Value; \
            name##Value = jsonDoc[#name]; \
            if (name##Value.isArray() && !name##Value.Empty()) { \
                const auto &name##Array = name##Value.toArray(); \
                if(name##Array.isEmpty()){ \
                    for(auto i = 0; i < name##Array.size(); i++){ \
                        m_##name.push_back(name##Value[i].toVariant().toDouble()); \
                    } \
                } \
            } \
        }


#define PARSE_JSON_STRING_LIST(name) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##Value; \
            name##Value = jsonDoc[#name]; \
            if (name##Value.isArray() && !name##Value.Empty()) { \
                const auto &name##Array = name##Value.toArray(); \
                if(name##Array.isEmpty()){ \
                    for(auto i = 0; i < name##Array.size(); i++){ \
                        m_##name.push_back(name##Value[i].toVariant().toString()); \
                    } \
                } \
            } \
        }

#define PARSE_JSON_BOOL_LIST(name) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##Value; \
            name##Value = jsonDoc[#name]; \
            if (name##Value.isArray() && !name##Value.Empty()) { \
                const auto &name##Array = name##Value.toArray(); \
                if(name##Array.isEmpty()){ \
                    for(auto i = 0; i < name##Array.size(); i++){ \
                        m_##name.push_back(name##Value[i].toVariant().toBool()); \
                    } \
                } \
            } \
        }

/*
* 不能使用rapidjson::Value name##JsonObj = jsonDoc[#name];，这样会调用复制, 而复制构造函数是private的，这样会报错
* 只能分开写成两行: rapidjson::Value name##JsonObj;
name##JsonObj = jsonDoc[#name];
*/
#define PARSE_JSON_OBJECT(name) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##JsonValue; \
            name##JsonValue = jsonObj[#name]; \
            if(name##JsonValue.isObject()) {  \
                m_##name.ParseJsonString(name##JsonValue.toObject()); \
            }  \
        }

#define PARSE_JSON_OBJECT_LIST(name, type) \
        if (jsonObj.contains(#name)) { \
            QJsonValue name##ObjArray; \
            name##ObjArray = jsonDoc[#name]; \
            if (name##ObjArray.isArray()) { \
                const auto &objArray = name##ObjArray.toArray();\
                if(!objArray.isEmpty()){ \
                    for(auto i = 0; i < objArray.size(); ++i){ \
                        QJsonValue name##Value; \
                        name##Value = name##ObjArray[i]; \
                        if(name##Value.isObject()){ \
                            type dataObj; \
                            dataObj.ParseJsonString(name##Value.toObject()); \
                            m_##name.push_back(dataObj);  \
                        } \
                    } \
                } \
            } \
        }


#define PARSE_JSON_SUB_OBJECT_END \
        return true; \
    }

#define PARSE_JSON_OBJECT_END \
        return true; \
    }

#endif // JSONTOOLS_H
