#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <QObject>
//获取对象单例
template<typename T>
class Singleton{
public:
    static T& getInstance(){
        static T t;
        return t;
    }
};

#define Provider(T) Singleton<T>::getInstance()

#endif // SINGLETON_HPP
