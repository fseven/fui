#ifndef KVCONFIG_H
#define KVCONFIG_H

#include <string>
#include "Singleton.hpp"

namespace FUI{
class IKVEditor;
class KVConfig
{
private:
    friend Singleton<KVConfig>;
    explicit KVConfig();
public:
    /**
     * 初始化业务KV数据库
     */
    static void init();

    /**
     * 初始化业务KV数据库
     * @param dir dir 工程主目录 需要全路径，比如：/root/fui/db
     */
    static void init(std::string dir);

    template <typename T>
    static void setValue(std::string key,T value);

    template <typename T>
    static T getValue(std::string key);
private:
    FUI::IKVEditor *m_editor = nullptr;
    std::string defaultPath;
};
}

#endif // KVCONFIG_H
