#include "KVConfig.h"
#include "DBKV.h"
#include "FileManager.h"
#include <QDebug>
#include <QCoreApplication>

using namespace FUI;

KVConfig::KVConfig()
{
    defaultPath = qApp->applicationDirPath().toStdString();
}

void KVConfig::init() {
    init(Provider(KVConfig).defaultPath);
}

void KVConfig::init(std::string dir) {
    KVConfig *d = &Provider(KVConfig);

    if(d->m_editor != nullptr) {
        return;
    }

    if(dir.empty()) {
        dir.append(d->defaultPath);
    }

    if(dir.at(dir.size() - 1) != '/') {
        dir = dir + "/";
    }

    dir.append("db/");
    Provider(FileManager).createDirWithCascade(QString::fromStdString(dir));
    dir.append("key_value.db");

    d->m_editor = new FUI::DBKV(dir);
}

template <typename T>
void KVConfig::setValue(std::string , T)
{
    static_assert(std::is_same<T, int>::value ||
            std::is_same<T, std::string>::value ||
            std::is_same<T, int64_t>::value ||
            std::is_same<T, float>::value ||
            std::is_same<T, double>::value,
            "Invalid type for setValue.");
}

template <>
void KVConfig::setValue<std::string>(std::string key,std::string value)
{
    KVConfig *d = &Provider(KVConfig);
    d->m_editor->setValue(key, value);
}

template <>
void KVConfig::setValue<int>(std::string key,int value)
{
    KVConfig *d = &Provider(KVConfig);
    d->m_editor->setInt(key, value);
}

template <>
void KVConfig::setValue<int64_t>(std::string key,int64_t value)
{
    KVConfig *d = &Provider(KVConfig);
    d->m_editor->setLong(key, value);
}

template <>
void KVConfig::setValue<float>(std::string key,float value)
{
    KVConfig *d = &Provider(KVConfig);
    d->m_editor->setFloat(key, value);
}

template <>
void KVConfig::setValue<double>(std::string key,double value)
{
    KVConfig *d = &Provider(KVConfig);
    d->m_editor->setDouble(key, value);
}

template<typename T>
T KVConfig::getValue(std::string key)
{
    static_assert(std::is_same<T, int>::value ||
            std::is_same<T, std::string>::value ||
            std::is_same<T, int64_t>::value ||
            std::is_same<T, float>::value ||
            std::is_same<T, double>::value,
            "Invalid type for setValue.");
    return getValue<T>(key);
}

template <>
std::string KVConfig::getValue<std::string>(std::string key)
{
    KVConfig *d = &Provider(KVConfig);
    return d->m_editor->getValue(key,"");
}

template <>
int KVConfig::getValue<int>(std::string key)
{
    KVConfig *d = &Provider(KVConfig);
    return d->m_editor->getInt(key,0);
}

template <>
int64_t KVConfig::getValue<int64_t>(std::string key)
{
    KVConfig *d = &Provider(KVConfig);
    return d->m_editor->getLong(key,0);
}

template <>
float KVConfig::getValue<float>(std::string key)
{
    KVConfig *d = &Provider(KVConfig);
    return d->m_editor->getFloat(key,0);
}

template <>
double KVConfig::getValue<double>(std::string key)
{
    KVConfig *d = &Provider(KVConfig);
    return d->m_editor->getDouble(key,0);
}
