#ifndef IKVEDITOR_H
#define IKVEDITOR_H

#include <string>
#include <unordered_map>

namespace FUI {
class IKVEditor {

public:
    IKVEditor() {}

    virtual ~IKVEditor() {}

public:
    /**
      * 设置键值对
      *
      * @param key 键
      * @param value 值
      * @return
      */
    virtual void setValue(const std::string &key, const std::string &value) = 0;

    virtual void setInt(const std::string &key, int value) = 0;

    virtual void setLong(const std::string &key, int64_t value) = 0;

    virtual void setFloat(const std::string &key, float value) = 0;

    virtual void setDouble(const std::string &key, double value) = 0;

    virtual void setBoolean(const std::string &key, bool value) = 0;

    /**
     * 获取键值
     *
     * @param key 键
     * @return 返回键值
     */
    virtual std::string getValue(const std::string &key, const std::string defaultValue) = 0;

    virtual int getInt(const std::string &key, int defaultValue) = 0;

    virtual int64_t getLong(const std::string &key, int64_t defaultValue) = 0;

    virtual float getFloat(const std::string &key, float defaultValue) = 0;

    virtual double getDouble(const std::string &key, double defaultValue) = 0;

    virtual bool getBoolean(const std::string &key, bool defaultValue) = 0;

    /**
     * 获取键总个数
     *
     * @return 返回键个数
     */
    virtual int storedKeysCount() = 0;

    /**
     * 删除单个键
     *
     * @param key 键
     * @return
     */
    virtual void remove(const std::string &key) = 0;

    /**
     * 删除所有键
     *
     * @return
     */
    virtual void clear() = 0;

    virtual std::unordered_map<std::string, std::string> getAll() = 0;
};
}
#endif // IKVEDITOR_H
