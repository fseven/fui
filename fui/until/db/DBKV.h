#ifndef DBKV_H
#define DBKV_H

#include <QString>
#include <mutex>

#include "sqlite_orm.h"
#include "IKVEditor.h"


using namespace sqlite_orm;
namespace FUI{
class DBKV : public IKVEditor {
        private:
            struct KeyValue {
                std::string key;
                std::string value;
            };

            template<class O, class T, class ...Op>
            using Column = internal::column_t<O, T, const T& (O::*)() const, void (O::*)(T), Op...>;

            using Storage = internal::storage_t<internal::table_t<KeyValue,
                    Column<KeyValue, decltype(KeyValue::key), constraints::primary_key_t<>>,
                    Column<KeyValue, decltype(KeyValue::value)>>>;

            std::unique_ptr<Storage> m_storage = nullptr;
            std::mutex m_mutexKV;
            std::string m_dbPath = "";
            std::unordered_map<std::string, std::string> m_allData;

        public:
            DBKV(const std::string &dbPath);

            ~DBKV();

        private:

            auto &getStorage();

        public:

            /**
             * 设置键值对
             *
             * @param key 键
             * @param value 值
             * @return
             */
            virtual void setValue(const std::string &key, const std::string &value) override;

            virtual void setInt(const std::string &key, int value) override;

            virtual void setLong(const std::string &key, int64_t value) override;

            virtual void setFloat(const std::string &key, float value) override;

            virtual void setDouble(const std::string &key, double value) override;

            virtual void setBoolean(const std::string &key, bool value) override;

            /**
             * 获取键值
             *
             * @param key 键
             * @return 返回键值
             */
            virtual std::string getValue(const std::string &key, const std::string defaultValue) override;

            virtual int getInt(const std::string &key, int defaultValue) override;

            virtual int64_t getLong(const std::string &key, int64_t defaultValue) override;

            virtual float getFloat(const std::string &key, float defaultValue) override;

            virtual double getDouble(const std::string &key, double defaultValue) override;

            virtual bool getBoolean(const std::string &key, bool defaultValue) override;

            /**
             * 获取键总个数
             *
             * @return 返回键个数
             */
            virtual int storedKeysCount() override;

            /**
             * 删除单个键
             *
             * @param key 键
             * @return
             */
            virtual void remove(const std::string &key) override;

            /**
             * 删除所有键
             *
             * @return
             */
            virtual void clear() override;

            virtual std::unordered_map<std::string, std::string> getAll() override;

        };

}


#endif // DBKV_H
