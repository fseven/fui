/*******************************************************************
 * @copyright  : 2018-2028,Shenzhen SenseTime Co.,Ltd
 * @file       : DBKV.cpp
 * @brief      : Description.
 * @author     : liuyi@sensetime.com
 * @date       : 2020-09-10 11:25 AM
 *******************************************************************/


#include "DBKV.h"
#include "FileManager.h"
#include <QDebug>

using namespace std;
using namespace FUI;

DBKV::DBKV(const string &dbPath) {
    m_dbPath = dbPath;
    Provider(FileManager).createDirWithCascade(QString::fromStdString(m_dbPath));
    if (m_dbPath.empty()) {
        m_dbPath.append("./key_value.db");
    }

    m_storage = std::make_unique<Storage>(make_storage(m_dbPath,
                                                       make_table("key_value",
                                                                  make_column("key", &KeyValue::key, primary_key()),
                                                                  make_column("value", &KeyValue::value))));
    try {
        m_storage->sync_schema();
        m_allData = getAll();
    } catch(std::system_error e) {
        qDebug()<<QString("DBKV::DBKV error code:%1, what:%2").arg(e.code().value()).arg(e.what());
        Provider(FileManager).deleteFile(QString::fromStdString(m_dbPath));
    } catch(...){
        qDebug()<<"DBKV::DBKV error: unknown exception";
    }
}

DBKV::~DBKV() {

}

void DBKV::setValue(const std::string &key, const std::string &value) {
    unique_lock<mutex> lock(m_mutexKV);
    KeyValue kv{key, value};
    m_storage->replace(kv);
    m_allData[key] = value;
}

void DBKV::setInt(const std::string &key, int value) {
    setValue(key, to_string(value));
}

void DBKV::setLong(const std::string &key, int64_t value) {
    setValue(key, to_string(value));
}

void DBKV::setFloat(const std::string &key, float value) {
    setValue(key, to_string(value));
}

void DBKV::setDouble(const std::string &key, double value) {
    setValue(key, to_string(value));
}

void DBKV::setBoolean(const std::string &key, bool value) {
    setValue(key, to_string(value));
}

std::string DBKV::getValue(const std::string &key, const std::string defaultValue) {
    unique_lock<mutex> lock(m_mutexKV);
    std::unordered_map<std::string, std::string>::iterator iter = m_allData.find(key);
    if(iter != m_allData.end()) {
        return iter->second;
    } else {
        return defaultValue;
    }
}

int DBKV::getInt(const std::string &key, int defaultValue) {
    const std::string value = getValue(key, "");
    return value.size() > 0 ? std::atoi(value.c_str()) : defaultValue;
}

int64_t DBKV::getLong(const std::string &key, int64_t defaultValue) {
    const std::string value = getValue(key, "");
    return value.size() > 0 ? std::atoll(value.c_str()) : defaultValue;
}

float DBKV::getFloat(const std::string &key, float defaultValue) {
    const std::string value = getValue(key, "");
    return value.size() > 0 ? std::atof(value.c_str()) : defaultValue;
}

double DBKV::getDouble(const std::string &key, double defaultValue) {
    const std::string value = getValue(key, "");
    return value.size() > 0 ? std::atof(value.c_str()) : defaultValue;
}

bool DBKV::getBoolean(const std::string &key, bool defaultValue) {
    const std::string value = getValue(key, "");
    if (value.size() > 0) {
        if (0 == value.compare("1")) {
            return true;
        } else {
            return false;
        }
    }
    return defaultValue;
}

int DBKV::storedKeysCount() {
    unique_lock<mutex> lock(m_mutexKV);
    return m_storage->count<KeyValue>();
}

void DBKV::remove(const std::string &key) {
    unique_lock<mutex> lock(m_mutexKV);
    m_storage->remove<KeyValue>(key);
    m_allData.erase(key);
}

void DBKV::clear() {
    unique_lock<mutex> lock(m_mutexKV);
    m_storage->remove_all<KeyValue>();
    m_allData.clear();
}

std::unordered_map<std::string, std::string> DBKV::getAll() {
    unique_lock<mutex> lock(m_mutexKV);

    std::unordered_map<std::string, std::string> allKVMap;

    std::vector<KeyValue> allKVList = m_storage.get()->get_all<KeyValue>();
    for (auto &it: allKVList) {
        allKVMap[it.key] = it.value;
    }

    return allKVMap;
}
