#include "MouseTracker.h"
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>
FUI::MouseTracker::MouseTracker(QObject *parent) : QObject(parent)
{

}

bool FUI::MouseTracker::eventFilter(QObject *watched, QEvent *event)
{        
    if (event->type() == QEvent::MouseMove) {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        QPoint mousePos = mouseEvent->windowPos().toPoint();
        emit this->positionChanged(mousePos);
    }
    return QObject::eventFilter(watched,event);;
}
