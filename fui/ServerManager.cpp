#include "ServerManager.h"
#include <QtConcurrent>
#ifdef Q_OS_WIN
#include <chrono>
#include <windows.h>
#elif defined(Q_OS_UNIX)
#include <sys/types.h>
#include <signal.h>
#endif
#include <QDebug>
#include "protocols/ServiceProtocol.h"
#include "ServiceManager.h"

FUI::ServerManager::ServerManager(QObject *parent):
    QObject(parent),
    process(new QProcess(parent)),
    thread()
{
    QObject::connect(process.get(), &QProcess::started, this, [=](){
        qDebug()<<"process started";
        pid = process.get()->processId();
    },Qt::QueuedConnection);

    QObject::connect(process.get(), QOverload<int,QProcess::ExitStatus>::of(&QProcess::finished), this, [=](int exitCode,QProcess::ExitStatus exitStatus){
        qDebug()<<QString("process finished: code %1    exitStatus %2").arg(exitCode).arg(exitStatus);
    },Qt::QueuedConnection);

    QObject::connect(process.get(),&QProcess::readyReadStandardOutput, this, [=](){
        const auto &output = process.get()->readAllStandardOutput();
        qDebug()<<"readyReadStandardOutput:"<<output;
        int startIndex = output.indexOf('{'); // 查找左大括号的索引
        int endIndex = output.lastIndexOf('}'); // 查找右大括号的索引
        if (startIndex != -1 && endIndex != -1 && endIndex > startIndex) {
            QString extractedData = output.mid(startIndex, endIndex - startIndex + 1).replace(" ", "").replace("'", "\"");; // 提取位于大括号之间的子字符串
            qDebug() << "Extracted Data: " << extractedData;
            ServiceProtocol<SEmptyData> emptyData;
            emptyData.ParseJsonString(extractedData.toLocal8Bit());
            qDebug()<< "ServiceProtocol emptyData: "<< emptyData.m_event;
            if(emptyData.m_event == "ready"){
                ServiceProtocol<SReady> readyProtocol;
                readyProtocol.ParseJsonString(extractedData.toLocal8Bit());
                emit this->ready(readyProtocol.m_data.m_ip,readyProtocol.m_data.m_port);
            }
        }
    },Qt::QueuedConnection);

    QObject::connect(process.get(), &QProcess::readyReadStandardError, this, [=](){
        qDebug()<<"readyReadStandardError:"<<process.get()->errorString();
    },Qt::QueuedConnection);

    QObject::connect(process.get(), QOverload<QProcess::ProcessError>::of(&QProcess::errorOccurred), this, [=](QProcess::ProcessError error){
        qDebug()<<"errorOccurred:"<<QString("%1").arg(error);
    },Qt::QueuedConnection);

    connect(this,&FUI::ServerManager::ready,this,[](const QString &ip,int port){
        qDebug()<<"FUI::ServerManager::ready";
        QUrl url;
        url.setPort(port);
        url.setHost(ip);
        Provider(ServiceManager).connectServer(url);
    });
}

void FUI::ServerManager::requestProcessTermination(const QString &processName)
{
    QScopedPointer<QProcess> p(new QProcess);
    QString command;

#ifdef Q_OS_WIN
    // 在Windows上使用TerminateProcess函数终止进程
    command = QString("taskkill /IM %1* /F").arg(processName);
#elif defined(Q_OS_UNIX)
    command = QString("pgrep %1* | xargs kill").arg(processName);
#endif
    p->start(command);
    p->waitForFinished();
}

FUI::ServerManager::~ServerManager()
{
    appDaemon = false;
    thread->wait();
    qDebug()<<"thread: finished";
}

void FUI::ServerManager::startServer()
{    
    if(thread.isNull()){
        thread.reset(QThread::create([&](){
            //防止程序上次被强制关闭，没有清理子进程
            auto result = isProcessRunning(processName);
            if(result){
                requestProcessTermination(processName);
            }

            do{
                auto result = isProcessRunning(processName);
                if(!result){
                    if(process->state() == QProcess::NotRunning){
                        process->setProgram(processName);
                        process->start();
                    } else{
                        process->kill();
                        process->waitForFinished();
                    }
                }
                QCoreApplication::processEvents();
                std::this_thread::sleep_for(std::chrono::seconds(5));
            }
            while(appDaemon);
            process->kill();
            process->waitForFinished();
            process->deleteLater();
        }));
    }
    process->moveToThread(thread.get());
    thread->start();
}

bool FUI::ServerManager::isProcessRunning(const QString& processName) {
    QScopedPointer<QProcess> p(new QProcess);
    QString command;

#ifdef Q_OS_WIN
    command = QString("tasklist /FI \"IMAGENAME eq %1*\"").arg(processName);
#elif defined(Q_OS_UNIX)
    command = QString("ps -C %1").arg(processName);
#endif
    p->start(command);
    p->waitForFinished();
    QString outputStr = QString::fromLocal8Bit(p->readAllStandardOutput());
    return outputStr.contains(processName);
}
