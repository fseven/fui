import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import fui 1.0
import route 1.0
import "component"
Column{
    TopArea{
        title: qsTr("FUI")
        width: parent.width
        height: 80
    }
    Control{
        height: parent.height - 80
        width: 1140
        contentItem: Row{
            LeftArea{
                width: 240
                height: parent.height
                pageData: Singleton.leftAreaData
            }
            RightArea{
                width: 1140 - 240
                height: parent.height
                initialItem: Route.homePage
            }
        }
    }
}

