import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

Control{
    id: _leftArea
    property var pageData
    Flickable{
        anchors.fill: parent
        clip: true

        ScrollBar.vertical: ScrollBar{
            id: _scrollBar
            active: false
            visible: false
            contentItem: Rectangle {
                implicitWidth: 6
                implicitHeight: 100
                radius: width / 2
                color: _scrollBar.hovered ? Qt.rgba(144/255,147/255,153/255,0.5) : Qt.rgba(144/255,147/255,153/255,0.3)
            }
        }

        contentHeight: _bottomPosition.y + _bottomPosition.height + 50
        Column{
           Repeater{
                id: _repeater
                function addObjectToList(objectList) {
                    objectList.forEach(function(object) {
                        console.log(JSON.stringify(object))
                        _listModel.append({
                            "title": object.title,
                            "items": object.items
                        })
                    })
                }
                model: ListModel{
                    id: _listModel
                }
                delegate: Column{
                    id: _navContent
                    property int index: model.index
                    Item{
                        width: parent.width
                        height: 15
                    }
                    Label{
                        id: _navTitle
                        height: 40
                        lineHeight: 26
                        lineHeightMode: Text.FixedHeight
                        font{
                            pixelSize: 12
                        }
                        text: model.title
                        color: "#999"
                    }
                    Repeater{
                        id: _navView
                        function addObjectToList(objectList) {
                            objectList.forEach(function(object) {
                                _listModel2.append({
                                    "name": object.name
                                })
                            })
                        }
                        model: ListModel{
                            id: _listModel2
                        }
                        delegate: Button{
                            id: _button
                            width: _leftArea.width
                            leftPadding: 0
                            contentItem: Label{
                                padding: 0
                                height: 40
                                lineHeight: 40
                                lineHeightMode: Text.FixedHeight
                                font{
                                    pixelSize: 14
                                }
                                text: model.name
                                color: (_button.hovered | _button.focus) ? "#409eff" : "#444"
                            }
                            background: Item{}
                            onClicked: {
                                pageData[_navContent.index].items[model.index].onClicked();
                            }
                        }
                        Component.onCompleted: {
                            _navView.addObjectToList(pageData[_navContent.index].items)
                        }
                    }
                }
                Component.onCompleted: {
                    _repeater.addObjectToList(pageData)
                }
            }
           Item {
                id: _bottomPosition
                height: 1
                width: _leftArea.width
            }
        }



    }

    MouseArea{
        id: _mouseArea
        Connections{
            target: mouseTracker
            onPositionChanged:{
//                console.log(`pos:${pos}`)
                let r = _leftArea.mapFromGlobal(pos.x,pos.y)
                if(r.x >= 0 && r.x <= _leftArea.width && r.y >= 0 && r.y <= _leftArea.height){
//                    console.log(`r.x:${r.x} r.y:${r.y} _leftArea.width:${_leftArea.width} _leftArea.height:${_leftArea.height}`)
                    _scrollBar.visible = true
//                    _mouseArea.cursorShape = Qt.PointingHandCursor
                } else{
                    _scrollBar.visible = false
                    _mouseArea.cursorShape = Qt.ArrowCursor
                }
            }
        }
        enabled: false
        anchors.fill: parent
    }
}



