import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import fui 1.0
import route 1.0
Item {
    property string title: qsTr("FUI")
    FTextButton{
        anchors.verticalCenter: parent.verticalCenter
        text: title
        color: "#1989fa"
        font{
            pixelSize: 60
        }
        onClicked: {
            Route.toHome()
        }
    }
    Rectangle{
        width: parent.width
        height: 1
        anchors.bottom: parent.bottom
        color: "#dcdfe6"
    }
}
