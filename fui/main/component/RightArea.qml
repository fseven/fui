import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
import route 1.0

StackView {
    id: _stackView
    focus: true
    Component.onCompleted: {
        Route.init(_stackView)
    }
}
