#ifndef SERVICEMANAGER_H
#define SERVICEMANAGER_H

#include <QObject>
#include "Singleton.hpp"
#include <functional>

class QWebSocket;
class ServiceManagerPrivate;

namespace FUI{
class ServiceManager : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ServiceManager)
    Q_DISABLE_COPY(ServiceManager)
    typedef std::function<void (const std::string &)> Callback;
private:
    friend Singleton<ServiceManager>;
    explicit ServiceManager(QObject *parent = nullptr);
    void connectServer();
    ServiceManagerPrivate* d_ptr;
public:
    void connectServer(const QUrl &url);
    void appendListener(const std::string &event,Callback callback);
    bool isConnected();
    void sendMessage(const QByteArray &msg);
signals:

public slots:
    void onConnected();
    void onClosed();
    void onError();
    void onDisconnected();
    void onTextMessageReceived(const QString& message);
};
}

#endif // SERVICEMANAGER_H
