﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include "until/GlobalFunction.h"
#include "until/MouseTracker.h"
#include "FLog.h"
#include "ServerManager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    //初始化日志系统
    Provider(FUI::FLog).initLog();
    //安装日志系统
    qInstallMessageHandler(FUI::FLog::messageHandler);

    Provider(FUI::ServerManager).startServer();

    QQmlApplicationEngine engine;    
    engine.addImportPath("qrc:/");
    engine.addImportPath("qrc:/export");

    qDebug()<<engine.importPathList();
    app.installEventFilter(&Provider(FUI::MouseTracker));
    FUI::registerManager(dynamic_cast<QQmlEngine*>(&engine));
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
