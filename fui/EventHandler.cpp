#include "EventHandler.h"
#include <memory>

EventHandler::EventHandler()
{
    dispatcher = std::make_shared<EventEmitter>();
}

void EventHandler::appendListener(Event event, Callback callback)
{
    dispatcher->appendListener(event,callback);
}

void EventHandler::dispatch(Event event, Body body)
{
    dispatcher->dispatch(event,body);
}
