﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
import button.style 1.0
Button{
    id: _button

    //获取背景色
    function getBackgroundColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_backgroundColor_hover :
                                 Plain.button_default_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_default_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_default_backgroundColor_hover :
                                 Normal.button_default_backgroundColor)
            }
        case FUI.Primary:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_primary_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_primary_backgroundColor_hover :
                                 Plain.button_primary_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_primary_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_primary_backgroundColor_hover :
                                 Normal.button_primary_backgroundColor)
            }

        case FUI.Success:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_success_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_success_backgroundColor_hover :
                                 Plain.button_success_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_success_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_success_backgroundColor_hover :
                                 Normal.button_success_backgroundColor)
            }
        case FUI.Info:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_info_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_info_backgroundColor_hover :
                                 Plain.button_info_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_info_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_info_backgroundColor_hover :
                                 Normal.button_info_backgroundColor)

            }

        case FUI.Warning:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_warning_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_warning_backgroundColor_hover :
                                 Plain.button_warning_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_warning_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_warning_backgroundColor_hover :
                                 Normal.button_warning_backgroundColor)
            }

        case FUI.Danger:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_danger_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_danger_backgroundColor_hover :
                                 Plain.button_danger_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_danger_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_danger_backgroundColor_hover :
                                 Normal.button_danger_backgroundColor)
            }

        default:
        case FUI.Default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_backgroundColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_backgroundColor_hover :
                                 Plain.button_default_backgroundColor)
            } else{
                return _button.pressed ?
                            Normal.button_default_backgroundColor_active :
                            (_button.hovered ?
                                 Normal.button_default_backgroundColor_hover :
                                 Normal.button_default_backgroundColor)
            }
        }
    }

    function getBorderColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_borderColor_hover :
                                 Plain.button_default_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_default_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_default_borderColor_hover :
                                 Normal.button_default_borderColor)
            }
        case FUI.Primary:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_primary_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_primary_borderColor_hover :
                                 Plain.button_primary_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_primary_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_primary_borderColor_hover :
                                 Normal.button_primary_borderColor)
            }

        case FUI.Success:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_success_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_success_borderColor_hover :
                                 Plain.button_success_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_success_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_success_borderColor_hover :
                                 Normal.button_success_borderColor)
            }
        case FUI.Info:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_info_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_info_borderColor_hover :
                                 Plain.button_info_borderColor)
                } else{
                return _button.pressed ?
                            Normal.button_info_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_info_borderColor_hover :
                                 Normal.button_info_borderColor)

            }

        case FUI.Warning:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_warning_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_warning_borderColor_hover :
                                 Plain.button_warning_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_warning_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_warning_borderColor_hover :
                                 Normal.button_warning_borderColor)
            }

        case FUI.Danger:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_danger_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_danger_borderColor_hover :
                                 Plain.button_danger_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_danger_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_danger_borderColor_hover :
                                 Normal.button_danger_borderColor)

            }

        default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_borderColor_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_borderColor_hover :
                                 Plain.button_default_borderColor)
            } else{
                return _button.pressed ?
                            Normal.button_default_borderColor_active :
                            (_button.hovered ?
                                 Normal.button_default_borderColor_hover :
                                 Normal.button_default_borderColor)
            }
        }
    }

    //获取字体颜色
    function getColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_color_hover :
                                 Plain.button_default_color)
            } else{
                return _button.pressed ?
                            Normal.button_default_color_active :
                            (_button.hovered ?
                                 Normal.button_default_color_hover :
                                 Normal.button_default_color)

            }

        case FUI.Primary:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_primary_color_hover :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_primary_color_hover :
                                 Plain.button_primary_color_hover)
            } else{
                return _button.pressed ?
                            Normal.button_primary_color_hover :
                            (_button.hovered ?
                                 Normal.button_primary_color_hover :
                                 Normal.button_primary_color_hover)
            }

        case FUI.Success:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_success_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_success_color :
                                 Plain.button_success_color)
            } else{
                return _button.pressed ?
                            Normal.button_success_color_active :
                            (_button.hovered ?
                                 Normal.button_success_color :
                                 Normal.button_success_color)
            }
        case FUI.Info:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_info_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_info_color :
                                 Plain.button_info_color)
            } else{
                return _button.pressed ?
                            Normal.button_info_color_active :
                            (_button.hovered ?
                                 Normal.button_info_color :
                                 Normal.button_info_color)

            }

        case FUI.Warning:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_warning_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_warning_color :
                                 Plain.button_warning_color)
            } else{
                return _button.pressed ?
                            Normal.button_warning_color_active :
                            (_button.hovered ?
                                 Normal.button_warning_color :
                                 Normal.button_warning_color)
            }

        case FUI.Danger:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_danger_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_danger_color :
                                 Plain.button_danger_color)
            } else{
                return _button.pressed ?
                            Normal.button_danger_color_active :
                            (_button.hovered ?
                                 Normal.button_danger_color :
                                 Normal.button_danger_color)

            }

        default:
            if(_button.isPlain){
                return (_button.pressed) ?
                            Plain.button_default_color_active :
                            ((_button.hovered || _button.focus) ?
                                 Plain.button_default_color :
                                 Plain.button_default_color)
            } else{
                return _button.pressed ?
                            Normal.button_default_color_active :
                            (_button.hovered ?
                                 Normal.button_default_color :
                                 Normal.button_default_color)

            }
        }
    }


    property int type: FUI.Default
    property bool isPlain: false
    property int letterSpacing: 0
    topPadding: 12
    bottomPadding: 12
    leftPadding: 12
    rightPadding: 12
    font.pixelSize: 14
    focus: Qt.ClickFocus

    onHoveredChanged: {
        if(hovered){
            _mouseArea.cursorShape = Qt.PointingHandCursor
        } else{
            _mouseArea.cursorShape = Qt.ArrowCursor
        }
    }

    FontLoader {
        id: _fontLoader
        source: "qrc:/assets/font/element-icons.732389d.ttf"
    }

    background: Rectangle{
        border.width: 1
        border.color: getBorderColor()
        radius: width / 2
        color: getBackgroundColor()
    }
    contentItem: Label {
        id: _text
        anchors.centerIn: parent
        font.family: _fontLoader.name
        font.pixelSize: 14
        lineHeight: 14
        lineHeightMode: Text.FixedHeight
        verticalAlignment: Text.AlignVCenter
        color: getColor()
        text: _button.text
    }

    MouseArea{
        id: _mouseArea
        enabled: false
        anchors.fill: parent
    }

    Component.onCompleted: {

    }
}

