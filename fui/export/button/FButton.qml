﻿import QtQuick 2.12
import QtQml 2.12
import QtQuick.Controls 2.12
import fui 1.0
import button.style 1.0
Button{
    id: _button
    //获取背景色
    function getBackgroundColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_backgroundColor_hover :
                                      Plain.button_default_backgroundColor)) :
                            Plain.button_default_backgroundColor_disable
            } else{
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Normal.button_default_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_default_backgroundColor_hover :
                                      Normal.button_default_backgroundColor)) :
                            Normal.button_default_backgroundColor_disable
            }
        case FUI.Primary:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_primary_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_primary_backgroundColor_hover :
                                      Plain.button_primary_backgroundColor)) :
                            Plain.button_primary_backgroundColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_primary_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_primary_backgroundColor_hover :
                                      Normal.button_primary_backgroundColor)) :
                            Normal.button_primary_backgroundColor_disable
            }

        case FUI.Success:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_success_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_success_backgroundColor_hover :
                                      Plain.button_success_backgroundColor)) :
                            Plain.button_success_backgroundColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_success_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_success_backgroundColor_hover :
                                      Normal.button_success_backgroundColor)) :
                            Normal.button_success_backgroundColor_disable
            }
        case FUI.Info:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_info_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_info_backgroundColor_hover :
                                      Plain.button_info_backgroundColor)) :
                            Plain.button_info_backgroundColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_info_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_info_backgroundColor_hover :
                                      Normal.button_info_backgroundColor)) :
                            Normal.button_info_backgroundColor_disable

            }

        case FUI.Warning:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_warning_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_warning_backgroundColor_hover :
                                      Plain.button_warning_backgroundColor)) :
                            Plain.button_warning_backgroundColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_warning_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_warning_backgroundColor_hover :
                                      Normal.button_warning_backgroundColor)) :
                            Normal.button_warning_backgroundColor_disable
            }

        case FUI.Danger:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_danger_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_danger_backgroundColor_hover :
                                      Plain.button_danger_backgroundColor)) :
                            Plain.button_danger_backgroundColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_danger_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_danger_backgroundColor_hover :
                                      Normal.button_danger_backgroundColor)) :
                            Normal.button_danger_backgroundColor_disable
            }

        default:
        case FUI.Default:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_backgroundColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_backgroundColor_hover :
                                      Plain.button_default_backgroundColor)) :
                            Plain.button_default_backgroundColor_disable
            } else{
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Normal.button_default_backgroundColor_active :
                                 (_button.hovered ?
                                      Normal.button_default_backgroundColor_hover :
                                      Normal.button_default_backgroundColor)) :
                            Normal.button_default_backgroundColor_disable
            }
        }
    }

    function getBorderColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_borderColor_hover :
                                      Plain.button_default_borderColor)) :
                            Plain.button_default_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_default_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_default_borderColor_hover :
                                      Normal.button_default_borderColor)) :
                            Normal.button_default_borderColor_disable
            }
        case FUI.Primary:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_primary_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_primary_borderColor_hover :
                                      Plain.button_primary_borderColor)) :
                            Plain.button_primary_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_primary_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_primary_borderColor_hover :
                                      Normal.button_primary_borderColor)) :
                            Normal.button_primary_borderColor_disable
            }

        case FUI.Success:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_success_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_success_borderColor_hover :
                                      Plain.button_success_borderColor)) :
                            Plain.button_success_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_success_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_success_borderColor_hover :
                                      Normal.button_success_borderColor)) :
                            Normal.button_success_borderColor_disable
            }
        case FUI.Info:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_info_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_info_borderColor_hover :
                                      Plain.button_info_borderColor)) :
                            Plain.button_info_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_info_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_info_borderColor_hover :
                                      Normal.button_info_borderColor)) :
                            Normal.button_info_borderColor_disable

            }

        case FUI.Warning:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_warning_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_warning_borderColor_hover :
                                      Plain.button_warning_borderColor)) :
                            Plain.button_warning_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_warning_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_warning_borderColor_hover :
                                      Normal.button_warning_borderColor)) :
                            Normal.button_warning_borderColor_disable
            }

        case FUI.Danger:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_danger_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_danger_borderColor_hover :
                                      Plain.button_danger_borderColor)) :
                            Plain.button_danger_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_danger_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_danger_borderColor_hover :
                                      Normal.button_danger_borderColor)) :
                            Normal.button_danger_borderColor_disable

            }

        default:
            if(_button.isPlain){
                return _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_borderColor_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_borderColor_hover :
                                      Plain.button_default_borderColor)) :
                            Plain.button_default_borderColor_disable
            } else{
                return _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_default_borderColor_active :
                                 (_button.hovered ?
                                      Normal.button_default_borderColor_hover :
                                      Normal.button_default_borderColor)) :
                            Normal.button_default_borderColor_disable
            }
        }
    }

    //获取字体颜色
    function getColor(){
        switch(_button.type){
        case FUI.Default:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_color_hover :
                                      Plain.button_default_color)) :
                            Plain.button_default_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_default_color_active :
                                 (_button.hovered ?
                                      Normal.button_default_color_hover :
                                      Normal.button_default_color)) :
                            Normal.button_default_color_disable

            }

        case FUI.Primary:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_primary_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_primary_color_hover :
                                      Plain.button_primary_color)):
                            Plain.button_primary_color_disable
            } else{
                return  _button.enabled ?
                            ( _button.pressed ?
                                 Normal.button_primary_color_active :
                                 (_button.hovered ?
                                      Normal.button_primary_color_hover :
                                      Normal.button_primary_color)) :
                            Normal.button_primary_color_disable
            }

        case FUI.Success:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_success_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_success_color_hover :
                                      Plain.button_success_color)) :
                            Plain.button_success_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_success_color_active :
                                 (_button.hovered ?
                                      Normal.button_success_color_hover :
                                      Normal.button_success_color)) :
                            Normal.button_success_color_disable
            }
        case FUI.Info:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_info_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_info_color_hover :
                                      Plain.button_info_color)) :
                            Plain.button_info_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_info_color_active :
                                 (_button.hovered ?
                                      Normal.button_info_color_hover :
                                      Normal.button_info_color)) :
                            Normal.button_info_color_disable
            }

        case FUI.Warning:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_warning_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_warning_color_hover :
                                      Plain.button_warning_color)) :
                            Plain.button_warning_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_warning_color_active :
                                 (_button.hovered ?
                                      Normal.button_warning_color_hover :
                                      Normal.button_warning_color)) :
                            Normal.button_warning_color_disable
            }

        case FUI.Danger:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_danger_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_danger_color_hover :
                                      Plain.button_danger_color)) :
                            Plain.button_danger_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_danger_color_active :
                                 (_button.hovered ?
                                      Normal.button_danger_color_hover :
                                      Normal.button_danger_color)) :
                            Normal.button_danger_color_disable

            }

        default:
            if(_button.isPlain){
                return  _button.enabled ?
                            ((_button.pressed) ?
                                 Plain.button_default_color_active :
                                 ((_button.hovered || _button.focus) ?
                                      Plain.button_default_color_hover :
                                      Plain.button_default_color)) :
                            Plain.button_default_color_disable
            } else{
                return  _button.enabled ?
                            (_button.pressed ?
                                 Normal.button_default_color_active :
                                 (_button.hovered ?
                                      Normal.button_default_color_hover :
                                      Normal.button_default_color)) :
                            Normal.button_default_color_disable

            }
        }
    }


    property int type: FUI.Default
    property bool isPlain: false
    property bool isRound: false
    property string iconText: ""
    property bool iconRight: false
    property int letterSpacing: 0
    topPadding: 12
    bottomPadding: 12
    leftPadding: 20
    rightPadding: 20
    font.pixelSize: 14
    focus: Qt.ClickFocus

    onHoveredChanged: {
        if(hovered){
            if(_button.enabled){
                _mouseArea.cursorShape = Qt.PointingHandCursor
            } else{
                _mouseArea.cursorShape = Qt.ForbiddenCursor
            }
        } else{
            _mouseArea.cursorShape = Qt.ArrowCursor
        }
    }

    background: Rectangle{
        border.width: 1
        border.color: getBorderColor()
        radius: _button.isRound ? 20 : 4
        color: getBackgroundColor()
    }
    contentItem: Row{
        spacing: 5
        FontLoader {
            id: _fontLoader
            source: "qrc:/assets/font/element-icons.732389d.ttf"
        }
        Label{
            font.family: _fontLoader.name
            anchors.verticalCenter: parent.verticalCenter
            visible: !iconRight
            lineHeight: 1
            color: getColor()
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: _button.font.pixelSize
            text: !iconRight ? _button.iconText : ""
        }
        Label{
            id: _text
            lineHeight: 1
            color: getColor()
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: _button.font.pixelSize
            text: _button.text
            font.letterSpacing: letterSpacing
        }
        Label{
            font.family: _fontLoader.name
            anchors.verticalCenter: parent.verticalCenter
            visible: iconRight
            lineHeight: 1
            color: getColor()
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: _button.font.pixelSize
            text: iconRight ? _button.iconText : ""
        }
    }

    MouseArea{
        id: _mouseArea
        enabled: false
        anchors.fill: parent
    }

    Component.onCompleted: {

    }
}

