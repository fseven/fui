import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
import button.style 1.0
Button{
    id: _button
    //获取背景色
    function getBackgroundColor(){
        return "#00000000"
    }

    function getBorderColor(){
        return "#00000000"
    }

    //获取字体颜色
    function getColor(){
        return _button.enabled ? "#409eff" : "#c0c4cc"
    }


    property int letterSpacing: 0
    property string color
    topPadding: 12
    bottomPadding: 12
    leftPadding: 20
    rightPadding: 20
    font.pixelSize: 14
    focus: Qt.ClickFocus

    onHoveredChanged: {
        if(hovered){
            if(_button.enabled){
                _mouseArea.cursorShape = Qt.PointingHandCursor
            } else{
                _mouseArea.cursorShape = Qt.ForbiddenCursor
            }
        } else{
            _mouseArea.cursorShape = Qt.ArrowCursor
        }
    }

    background: Rectangle{
        border.width: 1
        border.color: getBorderColor()
        color: getBackgroundColor()
    }
    contentItem: Label{
        id: _text
        lineHeight: 1
        color: _button.color ? _button.color : getColor()
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: _button.font.pixelSize
        text: _button.text
        font.letterSpacing: letterSpacing
    }

    MouseArea{
        id: _mouseArea
        enabled: false
        anchors.fill: parent
    }

    Component.onCompleted: {

    }
}

