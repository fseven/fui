﻿pragma Singleton
import QtQuick 2.12

QtObject {
    id: _config
    //FButton 的颜色
    readonly property string button_default_color_disable: "#c0c4cc"
    readonly property string button_default_borderColor_disable: "#ebeef5"
    readonly property string button_default_backgroundColor_disable: "#fff"

    readonly property string button_default_color: "#606266"
    readonly property string button_default_borderColor: "#dcdfe6"
    readonly property string button_default_backgroundColor: "#fff"

    readonly property string button_default_color_hover: "#409eff"
    readonly property string button_default_borderColor_hover: "#c6e2ff"
    readonly property string button_default_backgroundColor_hover: "#ecf5ff"
    
    readonly property string button_default_color_active: "#3a8ee6"
    readonly property string button_default_borderColor_active: "#3a8ee6"
    readonly property string button_default_backgroundColor_active: "#ecf5ff"

    readonly property string button_primary_color_disable: "#fff"
    readonly property string button_primary_borderColor_disable: "#a0cfff"
    readonly property string button_primary_backgroundColor_disable: "#a0cfff"

    readonly property string button_primary_color: "#fff"
    readonly property string button_primary_borderColor: "#409eff"
    readonly property string button_primary_backgroundColor: "#409eff"

    readonly property string button_primary_color_hover: "#fff"
    readonly property string button_primary_borderColor_hover: "#66b1ff"
    readonly property string button_primary_backgroundColor_hover: "#66b1ff"
    
    readonly property string button_primary_color_active: "#fff"
    readonly property string button_primary_borderColor_active: "#3a8ee6"
    readonly property string button_primary_backgroundColor_active: "#3a8ee6"

    readonly property string button_success_color_disable: "#fff"
    readonly property string button_success_borderColor_disable: "#b3e19d"
    readonly property string button_success_backgroundColor_disable: "#b3e19d"

    readonly property string button_success_color: "#fff"
    readonly property string button_success_borderColor: "#67c23a"
    readonly property string button_success_backgroundColor: "#67c23a"

    readonly property string button_success_color_hover: "#fff"
    readonly property string button_success_borderColor_hover: "#85ce61"
    readonly property string button_success_backgroundColor_hover: "#85ce61"
    
    readonly property string button_success_color_active: "#fff"
    readonly property string button_success_borderColor_active: "#5daf34"
    readonly property string button_success_backgroundColor_active: "#5daf34"

    readonly property string button_info_color_disable: "#fff"
    readonly property string button_info_borderColor_disable: "#c8c9cc"
    readonly property string button_info_backgroundColor_disable: "#c8c9cc"

    readonly property string button_info_color: "#fff"
    readonly property string button_info_borderColor: "#909399"
    readonly property string button_info_backgroundColor: "#909399"

    readonly property string button_info_color_hover: "#fff"
    readonly property string button_info_borderColor_hover: "#a6a9ad"
    readonly property string button_info_backgroundColor_hover: "#a6a9ad"

    readonly property string button_info_color_active: "#fff"
    readonly property string button_info_borderColor_active: "#82848a"
    readonly property string button_info_backgroundColor_active: "#82848a"

    readonly property string button_warning_color_disable: "#fff"
    readonly property string button_warning_borderColor_disable: "#f3d19e"
    readonly property string button_warning_backgroundColor_disable: "#f3d19e"

    readonly property string button_warning_color: "#fff"
    readonly property string button_warning_borderColor: "#e6a23c"
    readonly property string button_warning_backgroundColor: "#e6a23c"

    readonly property string button_warning_color_hover: "#fff"
    readonly property string button_warning_borderColor_hover: "#ebb563"
    readonly property string button_warning_backgroundColor_hover: "#ebb563"
    
    readonly property string button_warning_color_active: "#fff"
    readonly property string button_warning_borderColor_active: "#cf9236"
    readonly property string button_warning_backgroundColor_active: "#cf9236"

    readonly property string button_danger_color_disable: "#fff"
    readonly property string button_danger_borderColor_disable: "#fab6b6"
    readonly property string button_danger_backgroundColor_disable: "#fab6b6"

    readonly property string button_danger_color: "#fff"
    readonly property string button_danger_borderColor: "#f56c6c"
    readonly property string button_danger_backgroundColor: "#f56c6c"

    readonly property string button_danger_color_hover: "#fff"
    readonly property string button_danger_borderColor_hover: "#f78989"
    readonly property string button_danger_backgroundColor_hover: "#f78989"
    
    readonly property string button_danger_color_active: "#fff"
    readonly property string button_danger_borderColor_active: "#dd6161"
    readonly property string button_danger_backgroundColor_active: "#dd6161"
    // 在这里可以添加其他属性、方法或信号
}
