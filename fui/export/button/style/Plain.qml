﻿pragma Singleton
import QtQuick 2.12

QtObject {
    id: _config
    //FButton 的颜色
    readonly property string button_default_color_disable: "#c0c4cc"
    readonly property string button_default_borderColor_disable: "#ebeef5"
    readonly property string button_default_backgroundColor_disable: "#fff"

    readonly property string button_default_color: "#606266"
    readonly property string button_default_borderColor: "#dcdfe6"
    readonly property string button_default_backgroundColor: "#fff"

    readonly property string button_default_color_hover: "#409eff"
    readonly property string button_default_borderColor_hover: "#409eff"
    readonly property string button_default_backgroundColor_hover: "#fff"
    
    readonly property string button_default_color_active: "#3a8ee6"
    readonly property string button_default_borderColor_active: "#3a8ee6"
    readonly property string button_default_backgroundColor_active: "#fff"

    readonly property string button_primary_color_disable: "#8cc5ff"
    readonly property string button_primary_borderColor_disable: "#d9ecff"
    readonly property string button_primary_backgroundColor_disable: "#ecf5ff"

    readonly property string button_primary_color: "#409eff"
    readonly property string button_primary_borderColor: "#b3d8ff"
    readonly property string button_primary_backgroundColor: "#ecf5ff"

    readonly property string button_primary_color_hover: "#fff"
    readonly property string button_primary_borderColor_hover: "#409eff"
    readonly property string button_primary_backgroundColor_hover: "#409eff"
    
    readonly property string button_primary_color_active: "#fff"
    readonly property string button_primary_borderColor_active: "#3a8ee6"
    readonly property string button_primary_backgroundColor_active: "#3a8ee6"

    readonly property string button_success_color_disable: "#a4da89"
    readonly property string button_success_borderColor_disable: "#e1f3d8"
    readonly property string button_success_backgroundColor_disable: "#f0f9eb"

    readonly property string button_success_color: "#67c23a"
    readonly property string button_success_borderColor: "#c2e7b0"
    readonly property string button_success_backgroundColor: "#f0f9eb"

    readonly property string button_success_color_hover: "#fff"
    readonly property string button_success_borderColor_hover: "#67c23a"
    readonly property string button_success_backgroundColor_hover: "#67c23a"
    
    readonly property string button_success_color_active: "#fff"
    readonly property string button_success_borderColor_active: "#5daf34"
    readonly property string button_success_backgroundColor_active: "#5daf34"

    readonly property string button_info_color_disable: "#bcbec2"
    readonly property string button_info_borderColor_disable: "#e9e9eb"
    readonly property string button_info_backgroundColor_disable: "#f4f4f5"

    readonly property string button_info_color: "#909399"
    readonly property string button_info_borderColor: "#d3d4d6"
    readonly property string button_info_backgroundColor: "#f4f4f5"

    readonly property string button_info_color_hover: "#fff"
    readonly property string button_info_borderColor_hover: "#909399"
    readonly property string button_info_backgroundColor_hover: "#909399"

    readonly property string button_info_color_active: "#fff"
    readonly property string button_info_borderColor_active: "#82848a"
    readonly property string button_info_backgroundColor_active: "#82848a"

    readonly property string button_warning_color_disable: "#f0c78a"
    readonly property string button_warning_borderColor_disable: "#faecd8"
    readonly property string button_warning_backgroundColor_disable: "#fdf6ec"

    readonly property string button_warning_color: "#e6a23c"
    readonly property string button_warning_borderColor: "#f5dab1"
    readonly property string button_warning_backgroundColor: "#fdf6ec"

    readonly property string button_warning_color_hover: "#fff"
    readonly property string button_warning_borderColor_hover: "#e6a23c"
    readonly property string button_warning_backgroundColor_hover: "#e6a23c"
    
    readonly property string button_warning_color_active: "#fff"
    readonly property string button_warning_borderColor_active: "#cf9236"
    readonly property string button_warning_backgroundColor_active: "#cf9236"

    readonly property string button_danger_color_disable: "#f9a7a7"
    readonly property string button_danger_borderColor_disable: "#fde2e2"
    readonly property string button_danger_backgroundColor_disable: "#fef0f0"

    readonly property string button_danger_color: "#f56c6c"
    readonly property string button_danger_borderColor: "#fbc4c4"
    readonly property string button_danger_backgroundColor: "#fef0f0"

    readonly property string button_danger_color_hover: "#fff"
    readonly property string button_danger_borderColor_hover: "#f56c6c"
    readonly property string button_danger_backgroundColor_hover: "#f56c6c"
    
    readonly property string button_danger_color_active: "#fff"
    readonly property string button_danger_borderColor_active: "#dd6161"
    readonly property string button_danger_backgroundColor_active: "#dd6161"
    // 在这里可以添加其他属性、方法或信号
}
