import QtQuick 2.12
import QtQuick.Controls 2.12
Rectangle{
    id: _rec
    Connections{
        target: mouseTracker
        onPositionChanged:{
            let r = _rec.mapFromGlobal(pos.x,pos.y)
//            console.log(r)
            if(r.x >= 0 && r.y>= 0){
                _mouseArea.cursorShape = Qt.PointingHandCursor
            } else{
                _mouseArea.cursorShape = Qt.ArrowCursor
            }
        }
    }
    Column{
        anchors.fill: parent
        Button{
            id: _button
            width: parent.width
            height: 30
            background: Rectangle{
                color:  _button.hovered ? "red" : "pink"
            }
            contentItem: Label{
                text: "button1"
            }
            onClicked: {
                console.log(text)
            }
        }
        Button{
            width: parent.width
            height: 30
            text: "button2"
            onClicked: {
                console.log(text)
            }
        }

    }
    MouseArea{
        id: _mouseArea
        enabled: false
        anchors.fill: parent
    }

}
