import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Column{
    width: parent.width
    padding: 24
    Row{
        FButton{text:qsTr("默认按钮");enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("主要按钮");type:FUI.Primary;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("成功按钮");type:FUI.Success;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("信息按钮");type:FUI.Info;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("警告按钮");type:FUI.Warning;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("危险按钮");type:FUI.Danger;enabled: false}
    }
    SpacerItem{spacing: 20}
    Row{
        FButton{text:qsTr("朴素按钮");isPlain: true;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10;}
        FButton{text:qsTr("主要按钮");type:FUI.Primary;isPlain: true;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10;}
        FButton{text:qsTr("成功按钮");type:FUI.Success;isPlain: true;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10;}
        FButton{text:qsTr("信息按钮");type:FUI.Info;isPlain: true;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10;}
        FButton{text:qsTr("警告按钮");type:FUI.Warning;isPlain: true;enabled: false}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("危险按钮");type:FUI.Danger;isPlain: true;enabled: false}
    }
}
