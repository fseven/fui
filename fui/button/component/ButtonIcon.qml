import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Column{
    width: parent.width
    padding: 24
    Row{
        FButton{iconText: qsTr("\u{e78c}"); type:FUI.Primary}
        SpacerItem{direction: FUI.Horizontal; spacing: 10}
        FButton{iconText:qsTr("\u{e793}"); type:FUI.Primary}
        SpacerItem{direction: FUI.Horizontal; spacing: 10}
        FButton{iconText:qsTr("\u{e6d7}"); type:FUI.Primary}
        SpacerItem{direction: FUI.Horizontal; spacing: 10}
        FButton{iconText:qsTr("\u{e778}"); type:FUI.Primary; text:qsTr("搜索")}
        SpacerItem{direction: FUI.Horizontal; spacing: 10}
        FButton{iconText:qsTr("\u{e7c3}"); iconRight: true; text:qsTr("上传"); type:FUI.Primary}
    }
}
