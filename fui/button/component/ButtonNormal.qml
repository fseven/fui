import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Column{
    width: parent.width
    padding: 24
    Row{
        FButton{text:qsTr("默认按钮");}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("主要按钮");type:FUI.Primary}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("成功按钮");type:FUI.Success}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("信息按钮");type:FUI.Info}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("警告按钮");type:FUI.Warning}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("危险按钮");type:FUI.Danger}
    }
    SpacerItem{spacing: 20}
    Row{
        FButton{text:qsTr("朴素按钮");isPlain: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("主要按钮");type:FUI.Primary;isPlain: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("成功按钮");type:FUI.Success;isPlain: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("信息按钮");type:FUI.Info;isPlain: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("警告按钮");type:FUI.Warning;isPlain: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("危险按钮");type:FUI.Danger;isPlain: true}
    }
    SpacerItem{spacing: 20}
    Row{
        FButton{text:qsTr("圆角按钮");isRound: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("主要按钮");type:FUI.Primary;isRound: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("成功按钮");type:FUI.Success;isRound: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("信息按钮");type:FUI.Info;isRound: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("警告按钮");type:FUI.Warning;isRound: true}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButton{text:qsTr("危险按钮");type:FUI.Danger;isRound: true}
    }
    SpacerItem{spacing: 20}
    Row{
        FButtonCircle{text:"\u{e778}"}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButtonCircle{text:"\u{e78c}";type:FUI.Primary}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButtonCircle{text:"\u{e6da}";type:FUI.Success}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButtonCircle{text:"\u{e72b}";type:FUI.Info}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButtonCircle{text:"\u{e717}";type:FUI.Warning}
        SpacerItem{direction: FUI.Horizontal;spacing: 10}
        FButtonCircle{text:"\u{e6d7}";type:FUI.Danger}
    }
}
