﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
import "component"
BaseWindow{    
    objectName: "buttonPage"
    H2{
        text: qsTr("Button 按钮")
    }
    SpacerItem{
        spacing: 14
    }
    P{       
       text: qsTr("常用的操作按钮。")
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("基础用法")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("基础的按钮用法。")
    }
    SpacerItem{
        spacing: 14
    }

    DemoBlock{        
        source: ":/button/component/ButtonNormal.qml"
        sourceDescribe: qsTr('使用<font color="#e37f7d">type</font>、<font color="#e37f7d">isPlain</font>、<font color="#e37f7d">isRound</font>属性和控件<font color="#e37f7d">FButtonCircle</font>来定义 Button 的样式。')
        ButtonNormal{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("禁用状态")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("按钮不可用状态")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonDisable.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonDisable{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("文字按钮")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("没有边框和背景色的按钮。")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonText.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonText{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("图标按钮")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonIcon.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonIcon{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("按钮组")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("以按钮组的方式出现，常用于多项类似操作。")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonDisable.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonDisable{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("加载中")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("点击按钮后进行数据加载操作，在按钮上显示加载状态。")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonDisable.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonDisable{}
    }
    SpacerItem{
        spacing: 55
    }
    H3{
        text: qsTr("不同尺寸")
    }
    SpacerItem{
        spacing: 20
    }
    P{
        text: qsTr("Button 组件提供除了默认值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。")
    }
    SpacerItem{
        spacing: 14
    }
    DemoBlock{
        source: ":/button/component/ButtonDisable.qml"
        sourceDescribe: qsTr('你可以使用<font color="#e37f7d">enable</font>属性来定义按钮是否可用，它接受一个<font color="#e37f7d">bool</font>值。')
        ButtonDisable{}
    }
}
