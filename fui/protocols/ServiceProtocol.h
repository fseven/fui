#ifndef SERVICEPROTOCOL_H
#define SERVICEPROTOCOL_H

#include "until/json/JSONTools.h"

namespace FUI{
    template<class T>
    class ServiceProtocol{
    public:
        FORMAT_JSON_OBJECT_BEGIN
            FORMAT_JSON_STRING(event)
            FORMAT_JSON_OBJECT(data)
        FORMAT_JSON_OBJECT_END

        PARSE_JSON_OBJECT_BEGIN
            PARSE_JSON_STRING(event)
            PARSE_JSON_OBJECT(data)
        PARSE_JSON_OBJECT_END
    public:
        QString m_event = "command";
        T m_data;
    };

    class SEmptyData{
    public:
        FORMAT_JSON_SUB_OBJECT_BEGIN
        FORMAT_JSON_SUB_OBJECT_END

        PARSE_JSON_SUB_OBJECT_BEGIN
        PARSE_JSON_SUB_OBJECT_END
    };

    class SReady{
    public:
        FORMAT_JSON_SUB_OBJECT_BEGIN
            FORMAT_JSON_STRING(ip)
            FORMAT_JSON_NUMBER(port)
        FORMAT_JSON_SUB_OBJECT_END

        PARSE_JSON_SUB_OBJECT_BEGIN
            PARSE_JSON_STRING(ip)
            PARSE_JSON_UINT64(port)
        PARSE_JSON_SUB_OBJECT_END
    public:
        int m_port;
        QString m_ip;
    };
}

#endif // SERVICEPROTOCOL_H
