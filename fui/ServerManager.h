#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include "Singleton.hpp"
#include <QProcess>
#include <QThread>

namespace FUI {
class ServerManager :public QObject
{
    Q_OBJECT
private:
    explicit ServerManager(QObject *parent = nullptr);
    void requestProcessTermination(const QString &processName);
    bool isProcessRunning(const QString& processName);
    friend Singleton<ServerManager>;
public:
    ~ServerManager();
    void startServer();
signals:
    void ready(const QString &ip,int port);
private:
    QScopedPointer<QProcess> process;
    QScopedPointer<QThread>  thread;
    qint64 pid = 0;
    bool appDaemon = true;
    const QString processName = "fui-server";
};

}


#endif // SERVERMANAGER_H
