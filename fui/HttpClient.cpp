#include "HttpClient.h"

#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkConfiguration>
#include <QNetworkReply>
#include <QVector>

FUI::HttpClient::HttpClient(QObject *parent)
    : QObject(parent),
      accessManager(new QNetworkAccessManager(parent))
{    
    QNetworkConfiguration config;
    accessManager->setConfiguration(config);
}

QVector<QString> FUI::HttpClient::getHeaders()
{
    QVector<QString> headers;
    headers.push_back("Content-Type: application/json; charset=utf-8");
    return headers;
}

void FUI::HttpClient::setRequestHeadParameter(const HeadParameter &headParameter)
{
    this->headParameter = headParameter;
}

void FUI::HttpClient::get(const QNetworkRequest &request,QNetworkReply* reply)
{
    reply = accessManager->get(request);
    QEventLoop loop;
    connect(reply,&QNetworkReply::finished,&loop,&QEventLoop::quit);
    loop.exec();
    return;
}


