import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
FocusScope{
    id: _basePage
    objectName: "basePage"
    default property alias contentItem: _content.data
    property string backgroundImage: ""
    Control{
        anchors.fill: parent
        topPadding: 15
        leftPadding: 30
        background: Image{
            source: backgroundImage
        }
        contentItem: FocusScope{
            id: _focusScope
            focus: true
            clip: false
            Flickable{
                id: _flickable
                anchors.fill: parent
                clip: true
                contentHeight: _bottomPosition.y + _bottomPosition.height + 50
                ScrollBar.vertical: ScrollBar {
                    id: _scrollBar
                    opacity: 0
                    function doPositionChanged(){
                        if(_scrollBar.position !== Singleton.contentScrollBar.position){
                            _scrollBar.position = Singleton.contentScrollBar.position
                            console.log(_scrollBar.position)
                        }
                    }
                    onHeightChanged: {
                        Singleton.contentScrollBar.y = _scrollBar.mapToItem(Singleton.contentScrollBar,_scrollBar.x,_scrollBar.y).y
                        Singleton.contentScrollBar.height = _scrollBar.height
                    }
                    onPositionChanged: {
                        Singleton.contentScrollBar.position = _scrollBar.position
                    }
                    onSizeChanged: {
                        Singleton.contentScrollBar.size = _scrollBar.size
                    }
                    Component.onCompleted: {
                        Singleton.contentScrollBar.positionChanged.connect(doPositionChanged)
                    }
                    Component.onDestruction: {
                        Singleton.contentScrollBar.positionChanged.disconnect(doPositionChanged)
                    }
                }

                onContentHeightChanged: {
                    if(_flickable.height < _flickable.contentHeight){
                        Singleton.contentScrollBar.visible = true
                    } else{
                        Singleton.contentScrollBar.visible = false
                    }
                }

                Column{
                    id: _content
                    width: parent.width

                }
                Item{
                    id: _bottomPosition
                    anchors.top: _content.bottom
                    width: parent.width
                    height: 1
                }
            }

        }
    }
}
