import QtQuick 2.12
import fui 1.0
Item{
    property int direction: FUI.Vertical
    property int spacing: 0
    width: direction === FUI.Vertical ? parent.width : spacing
    height: direction === FUI.Horizontal ? parent.height : spacing
}
