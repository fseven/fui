import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Label{
    id: _label
    color: "#1f2f3d"
    lineHeight: 1.5 * 22
    lineHeightMode: Text.FixedHeight
    verticalAlignment: Text.AlignVCenter
    font{
        pixelSize: 22
    }
}
