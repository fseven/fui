import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Label{
    id: _label
    lineHeight: 37
    lineHeightMode: Text.FixedHeight
    verticalAlignment: Text.AlignVCenter
    color: "#1f2f3d"
    font{
        pixelSize: 28
    }
}
