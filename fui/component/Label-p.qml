import QtQuick 2.12
import QtQuick.Controls 2.12
import fui 1.0
Label{
    id: _label
    property int fontSize: 14
    color: "#5e6d82"
    lineHeight: 1.5 * _label.fontSize
    lineHeightMode: Text.FixedHeight
    verticalAlignment: Text.AlignVCenter
    font{
        pixelSize: _label.fontSize
    }
}
