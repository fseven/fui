import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQml 2.12
import fui 1.0
Control{
    id: _demoBlock
    default property alias contentArea: _content.data
    property string source
    property string sourceDescribe
    property bool isExpand: false
    width: parent.width
    bottomPadding: 1
    leftPadding: 1
    rightPadding: 1
    topPadding: 1
    background: Rectangle{
        border.width: 1
        border.color: "#ebebeb"
        color: "#fff"
    }
    contentItem: Column{
        Column{            
            width: parent.width
            Column{
                id: _content
                width: parent.width
            }
            Control{
                id: _meta
                width: parent.width
                visible: false
                height: _meta.implicitContentHeight
                padding: 10
                background: Rectangle{
                    color: "#fafafa"
                }
                contentItem: Column{
                    spacing: 10
                    Label{
                        id: _description
                        width: parent.width
                        padding: 20
                        lineHeight: 22
                        lineHeightMode: Text.FixedHeight
                        color: "#666"
                        font.pixelSize: 14
                        background: Rectangle{
                            color: "#fff"
                            border.color: "#ebebeb"
                            border.width: 1
                        }
                        text: _demoBlock.sourceDescribe
                    }
                    TextEdit{
                        id: _codeBlock
                        wrapMode: Text.WrapAnywhere
                        textFormat: Qt.RichText
                        readOnly: true
                        selectByMouse: true
                        font.pixelSize: 12
                        topPadding: 18
                        bottomPadding: 18
                        leftPadding: 24
                        rightPadding: 24
                        Component.onCompleted: {
                            if(_demoBlock.source && _demoBlock.source !== ""){
                                fileManager.readFileAsync(_demoBlock.source,function(result){
                                    _codeBlock.text = '<body style="line-height:1.8;">'  + result + '</body>'
                                })
                            } else{
                                console.info("代码块路径不能为空!!!")
                            }
                        }
                    }
                }
            }


        }
        Button{
            id: _button
            width: parent.width
            height: 43
            background: Rectangle{
                color: _button.hovered ? Qt.rgba(249/255,249/255,249/255,1) : Qt.rgba(1,1,1,1)
            }
            contentItem: Item{
                FontLoader {
                    id: _fontLoader
                    source: "qrc:/assets/font/element-icons.732389d.ttf"
                }
                Text {
                    anchors.centerIn: parent
                    font.family: _fontLoader.name
                    font.pixelSize: 16
                    lineHeight: 44
                    lineHeightMode: Text.FixedHeight
                    verticalAlignment: Text.AlignVCenter
                    color: _button.hovered ? "#409eff" : "#d3dce6"
                    text: _demoBlock.isExpand ? (_button.hovered ? "\u{e78f}" + qsTr(" 隐藏代码") : "\u{e78f}") : (_button.hovered ? "\u{e790}" + qsTr(" 显示代码") : "\u{e790}")
                }
            }
            onClicked: {
                console.log(`button clicked`)
                _demoBlock.isExpand = !_demoBlock.isExpand
                _meta.visible = !_meta.visible
            }
        }

    }
}
