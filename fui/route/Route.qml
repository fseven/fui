pragma Singleton

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQml 2.12


QtObject{
    function init(obj){
        _stackview = obj
    }

    function push(url,objectName){
        if(objectName && objectName !== ""){
            let page = _stackview.getPage(objectName)
            if(page){
                console.log(`push ${url} already exists`)
                _stackview.pop(page,StackView.Immediate)
                return
            }
        } else{
            _stackview.push(url,StackView.Immediate)
        }
    }

    function toHome(){
        _stackview.replace(null,Route.homePage,StackView.Immediate)
    }

    function getPage(objectName){
        let page = _stackview.find((item) => { return item.objectName === objectName })
        return page
    }

    function popTo(objectName,isContain = false){
        let currentPageName = _stackview.currentItem.objectName
        if(currentPageName === objectName){
            console.log("currentPageName equal target objectName")
            if(isContain){
                _stackview.pop(StackView.Immediate)
            }
            return
        }
        let page = _stackview.getPage(objectName)
        if(page){
            console.log(`pageTo ${page.objectName} isContain:${isContain}`)
            _stackview.pop(page,StackView.Immediate)
        } else{
            let home = _stackview.getPage("home")
            console.log(`error !!! ${objectName} does not exist!!!`)
            _stackview.pop(home,StackView.Immediate)
            return
        }

        if(isContain){
            _stackview.pop(StackView.Immediate)
        }
    }

    property var _stackview

    readonly property string homePage: "qrc:/home/HomePage.qml"
    readonly property string buttonPage: "qrc:/button/ButtonPage.qml"
}
