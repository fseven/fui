LIB_INSTALL_DIR=$$PWD/fui/lib
cache(LIB_INSTALL_DIR, super)

TEMPLATE      = subdirs

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SUBDIRS += fui/thirdParty/thirdParty.pro \
           fui/app.pro

CONFIG += order
