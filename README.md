# FUI

FUI 是仿照 [element-ui](https://element.eleme.io/#/zh-CN/component/installation) 开发的`qml UI`控件库，使用的是`Qt.5.12.11` `Mingw32` `qt creator 4.15.0`

如果有兴趣一起开发，可参考 `ButtonPage.qml` 页面结构。下面是一些目录介绍

`export 目录` 是最后需要导出的控件库的目录，可以把想要导出的控件放到此文件夹。

`route 目录` 是路由表配置目录，可以将开发完成的页面路径放在此配件文件中

`fui\qmldir` 文件是 qml 组件配置文件

`fui\route\qmldir` 文件是 qml 路由的配置文件

目前暂未开发完成。欢迎大家一起来完成 😊😊😊
